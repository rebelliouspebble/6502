﻿

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using SystemEmulator;
using ScintillaNET;
using Timer = System.Threading.Timer;

namespace IDE
{
    /**********************************************************************************************/
    /**
     * \class   ExecutionScreen
     *
     * \brief   Class ExecutionScreen. Implements the <see cref="System.Windows.Forms.Form" />
     *
     * \author  Glenn
     * \date    10/01/2020
     *
     * \sa  System.Windows.Forms.Form
     **************************************************************************************************/
    public partial class ExecutionScreen : Form
    {
        /** \brief   The emulator */
        Processor emulator;

        /** \brief   The emulator speed */
        private int speed;


        /**********************************************************************************************/
        /**
         * \fn  public ExecutionScreen(string binaryfile, int offset, int entry)
         *
         * \brief   Initializes a new instance of the <see cref="ExecutionScreen"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   binaryfile  The binaryfile.
         * \param   offset      The offset.
         * \param   entry       The entry.
         **************************************************************************************************/
        public ExecutionScreen(string binaryfile, int offset, int entry)
        {
            InitializeComponent();
            emulator = new Processor(300, new Memory());
            emulator.LoadProgram(binaryfile, offset, entry);

        }

        /**********************************************************************************************/
        /**
         * \fn  private void ExecutionScreen_Load(object sender, EventArgs e)
         *
         * \brief   Handles the Load event of the ExecutionScreen control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void ExecutionScreen_Load(object sender, EventArgs e)
        {
            //initiate page selector
            for (int i = 0; i <= 255; i++)
            {
                MemoryPageSelector.Items.Add(i.ToString());
            }

            InitMemGrid();
            MemoryPageSelector.SelectedIndex = 0;
            emulator.setupTimer();
            emulator.cpuclock.Elapsed += Instruction_Complete;
        }

        /**********************************************************************************************/
        /**
         * \fn  private void InitMemGrid()
         *
         * \brief   Initializes the memory grid view
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void InitMemGrid()
        {
            string Rows = "00,10,20,30,40,50,60,70,80,90,A0,B0,C0,D0,E0,F0";

            for (int i = 0; i < 16; i++)
            {
                memGrid.Rows.Add("00", "00", "00", "00", "00", "00", "00", "00", "00", "00", "00", "00", "00", "00",
                    "00", "00");
            }

            for (int i = 0; i < 16; i++)
            {
                memGrid.Rows[i].HeaderCell.Value = Rows.Split(',')[i];
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void UpdateMemory()
         *
         * \brief   Updates the memory grid view
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void UpdateMemory()
        {
            int startbyte = MemoryPageSelector.SelectedIndex * 0xFF;

            var pairs = from i in Enumerable.Range(0, 16)
                from j in Enumerable.Range(0, 16)
                select new {i = i, j = j};

            foreach (var p in pairs)
            {
                memGrid[p.i, p.j].Value = emulator.Memory.DirectReadMemory(startbyte);
                startbyte++;
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void UpdateRegisters()
         *
         * \brief   Updates the register view
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void UpdateRegisters()
        {
            txtPC.Text = emulator.ProgramCounter.ToString();
            txtA.Text = emulator.Accumulator.ToString();
            txtX.Text = emulator.IndexRegisterX.ToString();
            txtY.Text = emulator.IndexRegisterY.ToString();
            txtCyc.Text = emulator.cyclecount.ToString();
            var flags = emulator.statusToBitArray();
            for (int i = 0; i < 7; i++)
            {
                //
            }

            txtCI.Text = emulator.currentoperation.mme;
        }

        /**********************************************************************************************/
        /**
         * \fn  private void UpdateList()
         *
         * \brief   Updates the instruction list
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void UpdateList()
        {
            var index = instGrid.Rows.Add();
            Translator translator = new Translator();
            instGrid.Rows[index].Cells["Instruction"].Value = emulator.currentoperation.mme;
            instGrid.Rows[index].Cells["AddressingMode"].Value = emulator.currentoperation.AddressingMode;
            instGrid.Rows[index].Cells["Operand"].Value = emulator.currentoperand;
            instGrid.Rows[index].Cells["Information"].Value =
                translator.ReturnDescription(emulator.currentoperation.mme);
        }

        /**********************************************************************************************/
        /**
         * \fn  private void Instruction_Complete(object o, ElapsedEventArgs eventArgs)
         *
         * \brief   Event handler. Called by Instruction for complete events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   o           An object to process.
         * \param   eventArgs   Elapsed event information.
         **************************************************************************************************/
        private void Instruction_Complete(object o, ElapsedEventArgs eventArgs)
        {
            UpdateRegisters();
            UpdateMemory();
            UpdateList();
            //Thread.Sleep(10);
        }

        /**********************************************************************************************/
        /**
         * \fn  private void Instruction_Complete()
         *
         * \brief   Instruction complete for manual update
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void Instruction_Complete()
        {
            UpdateRegisters();
            UpdateMemory();
            UpdateList();
        }


        /**********************************************************************************************/
        /**
         * \fn  private void MemoryPageSelector_SelectedIndexChanged(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by MemoryPageSelector for selected index changed events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void MemoryPageSelector_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateMemory();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void btnStartExec_Click(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by btnStartExec for click events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void btnStartExec_Click(object sender, EventArgs e)
        {
            emulator.cpuclock.Start();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void btnStopExec_Click(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by btnStopExec for click events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void btnStopExec_Click(object sender, EventArgs e)
        {
            emulator.cpuclock.Stop();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void btnStepExec_Click(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by btnStepExec for click events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void btnStepExec_Click(object sender, EventArgs e)
        {
            emulator.TimerTick();
            Instruction_Complete();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void speedBar_Scroll(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by speedBar for scroll events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void speedBar_Scroll(object sender, EventArgs e)
        {
            emulator.clockSpeed = speedBar.Value * 10;
        }
    }
}