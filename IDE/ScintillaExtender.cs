/**********************************************************************************************//**
 * \file    ScintillaExtender.cs.
 *
 * \brief   Implements the scintilla extender class
 **************************************************************************************************/

using System;
using System.Drawing;
using SystemEmulator;
using ScintillaNET;

namespace IDE
{
    /**********************************************************************************************//**
     * \class   ScintillaExtender
     *
     * \brief   Class ScintillaExtender.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public static class ScintillaExtender
    {
        /**********************************************************************************************//**
         * \fn  public static void SetEditorStyle(this Scintilla txtScintilla, string fontname, int fontsize, bool viewspaces)
         *
         * \brief   Sets the editor style.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   txtScintilla    Scintilla text editor control
         * \param   fontname        The fontname.
         * \param   fontsize        The fontsize.
         * \param   viewspaces      if set to <c>true</c> [viewspaces].
         **************************************************************************************************/

        public static void SetEditorStyle(this Scintilla txtScintilla, string fontname, int fontsize,
            bool viewspaces)
        {
            var alphaChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            var numericChars = "0123456789";
            var accentedChars = "ŠšŒœŸÿÀàÁáÂâÃãÄäÅåÆæÇçÈèÉéÊêËëÌìÍíÎîÏïÐðÑñÒòÓóÔôÕõÖØøÙùÚúÛûÜüÝýÞþßö";

            // Configuring the default style with properties
            txtScintilla.StyleResetDefault();
            txtScintilla.Styles[Style.Default].Font = fontname;
            txtScintilla.Styles[Style.Default].Size = fontsize;

            txtScintilla.StyleClearAll();

            txtScintilla.WordChars = alphaChars + numericChars + accentedChars;


            txtScintilla.Lexer = Lexer.Asm;
            txtScintilla.SetKeywords(0,
                "LDA LDX LDY STA STX STY TAX TAY TXA TYA TSX TXS PHA PHP PLA PLP AND EOR ORA BIT ADC SBC CMP CPX CPY INC INX INY DEC DEX DEY ASL LSR ROL ROR JMP JSR RTS BCC BCS BEQ BMI BNE BPL BVC BVS CLC CLD CLI CLV SEC SED SEI BRK NOP RTI");
            txtScintilla.SetKeywords(2, "A X Y");
            txtScintilla.SetKeywords(3,
                ".outfile .advance .alias .byte .cmbfloat .checkpc .data .incbin .include .org .require .space .text .word .dword .wordbe .dwordbe .scope .scend .macro .macend .invoke");


            // Configure folding markers with respective symbols
            txtScintilla.Markers[Marker.Folder].Symbol = MarkerSymbol.BoxPlus;
            txtScintilla.Markers[Marker.FolderOpen].Symbol = MarkerSymbol.BoxMinus;
            txtScintilla.Markers[Marker.FolderEnd].Symbol = MarkerSymbol.BoxPlusConnected;
            txtScintilla.Markers[Marker.FolderMidTail].Symbol = MarkerSymbol.TCorner;
            txtScintilla.Markers[Marker.FolderOpenMid].Symbol = MarkerSymbol.BoxMinusConnected;
            txtScintilla.Markers[Marker.FolderSub].Symbol = MarkerSymbol.VLine;
            txtScintilla.Markers[Marker.FolderTail].Symbol = MarkerSymbol.LCorner;

// Enable automatic folding
            txtScintilla.AutomaticFold = AutomaticFold.Show | AutomaticFold.Click | AutomaticFold.Change;

            // Instruct the lexer to calculate folding
            txtScintilla.SetProperty("fold", "1");
            txtScintilla.SetProperty("fold.compact", "1");

// Configure a margin to display folding symbols
            txtScintilla.Margins[2].Type = MarginType.Symbol;
            txtScintilla.Margins[2].Mask = Marker.MaskFolders;
            txtScintilla.Margins[2].Sensitive = true;
            txtScintilla.Margins[2].Width = 20;

// Set colors for all folding markers
            for (var i = 25; i <= 31; i++)
            {
                txtScintilla.Markers[i].SetForeColor(SystemColors.ControlLightLight);
                txtScintilla.Markers[i].SetBackColor(SystemColors.ControlDark);
            }

            // Configure text styles
            txtScintilla.Styles[Style.Asm.Comment].Bold = true;
            txtScintilla.Styles[Style.Asm.Comment].ForeColor = Color.Green;
            txtScintilla.Styles[Style.Asm.Comment].Weight = 700;
            txtScintilla.Styles[Style.Asm.CommentBlock].Bold = true;
            txtScintilla.Styles[Style.Asm.CommentBlock].ForeColor = Color.Green;
            txtScintilla.Styles[Style.Asm.CommentBlock].Weight = 700;
            txtScintilla.Styles[Style.Asm.Number].ForeColor = Color.Violet;
            txtScintilla.Styles[Style.Asm.MathInstruction].ForeColor = Color.Blue;
            txtScintilla.Styles[Style.Asm.String].ForeColor = Color.OrangeRed;
            txtScintilla.Styles[Style.Asm.Character].ForeColor = Color.LightSalmon;
            txtScintilla.Styles[Style.Asm.CpuInstruction].Case = StyleCase.Upper;
            txtScintilla.Styles[Style.Asm.CpuInstruction].ForeColor = Color.Blue;
            txtScintilla.Styles[Style.Asm.Register].ForeColor = Color.Red;
            txtScintilla.Styles[Style.Asm.Operator].ForeColor = Color.Turquoise;
        }

        /**********************************************************************************************//**
         * \fn  public static void SetColumnMargins(this Scintilla scintilla)
         *
         * \brief   Sets the column margins.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   Scintilla text editor control
         **************************************************************************************************/

        public static void SetColumnMargins(this Scintilla scintilla)
        {
            var maxLineNumberCharLength = scintilla.Lines.Count.ToString().Length;
            var padding = 2;
            scintilla.Margins[0].Width =
                scintilla.TextWidth(Style.LineNumber, new string('9', maxLineNumberCharLength + 1)) + padding;
        }

        /**********************************************************************************************//**
         * \fn  public static void ShowAutoComplete(this Scintilla scintilla)
         *
         * \brief   Handles autocompletion
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   Scintilla text editor control
         **************************************************************************************************/

        public static void ShowAutoComplete(this Scintilla scintilla)
        {
            var suggestions =
                "LDA LDX LDY STA STX STY TAX TAY TXA TYA TSX TXS PHA PHP PLA PLP AND EOR ORA BIT ADC SBC CMP CPX CPY INC INX INY DEC DEX DEY ASL LSR ROL ROR JMP JSR RTS BCC BCS BEQ BMI BNE BPL BVC BVS CLC CLD CLI CLV SEC SED SEI BRK NOP RTI";

            var currentPos = scintilla.CurrentPosition;
            var wordStartPos = scintilla.WordStartPosition(currentPos, true);

            // Display the autocompletion list
            var lenEntered = currentPos - wordStartPos;
            if (lenEntered > 0)
                if (!scintilla.AutoCActive)
                    scintilla.AutoCShow(lenEntered, suggestions);
        }

        /**********************************************************************************************//**
         * \fn  public static string getLastWord(this Scintilla scintilla)
         *
         * \brief   Gets the last word.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   Scintilla text editor control
         *
         * \returns System.String.
         **************************************************************************************************/

        public static string getLastWord(this Scintilla scintilla)
        {
            var word = "";
            var pos = scintilla.SelectionStart;
            if (pos > 1)
            {
                var tmp = "";
                var f = new char();
                while ((f != ' ') & (pos > 0))
                {
                    pos -= 1;
                    tmp = scintilla.Text.Substring(pos, 1);
                    f = Convert.ToChar(tmp[0]);
                    word += f;
                }

                var ca = word.ToCharArray();
                Array.Reverse(ca);

                word = new string(ca);
            }

            return word.Trim();
        }

        /**********************************************************************************************//**
         * \fn  public static void ShowToolTip(this Scintilla scintilla)
         *
         * \brief   Shows the tool tip.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   Scintilla text editor control
         **************************************************************************************************/

        public static void ShowToolTip(this Scintilla scintilla)
        {
            var text = scintilla.getLastWord();


            var helptxt = "";

            if (text.Length == 3) helptxt = scintilla.FormatHelpTip(text);

            if (helptxt != "") scintilla.CallTipShow(scintilla.CurrentPosition, helptxt);
            else scintilla.CallTipCancel();
        }

        /**********************************************************************************************//**
         * \fn  public static string FormatHelpTip(this Scintilla scintilla, string inst)
         *
         * \brief   Generates and formats the help tip
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   Scintilla text editor control
         * \param   inst        The instruction.
         *
         * \returns System.String.
         **************************************************************************************************/

        public static string FormatHelpTip(this Scintilla scintilla, string inst)
        {
            var translator = new Translator();
            var helptext = "";
            var matches = false;

            var possibleInstructions =
                "LDA LDX LDY STA STX STY TAX TAY TXA TYA TSX TXS PHA PHP PLA PLP AND EOR ORA BIT ADC SBC CMP CPX CPY INC INX INY DEC DEX DEY ASL LSR ROL ROR JMP JSR RTS BCC BCS BEQ BMI BNE BPL BVC BVS CLC CLD CLI CLV SEC SED SEI BRK NOP RTI";


            foreach (var i in possibleInstructions.Split(' '))
                if (i.ToLower() == inst.ToLower())
                    matches = true;

            if (matches)
            {
                var instruction = translator.AllData.Instructions.Find(x => x.mnemonic.Contains(inst.ToUpper()));
                helptext += instruction.description;
                helptext += Environment.NewLine;
                helptext += "Possible Addressing Modes: ";
                helptext += Environment.NewLine;
                foreach (var i in instruction.Operations)
                {
                    helptext += "Mode: " + i.Value.adressingmode + " Use: " + i.Value.format;
                    helptext += Environment.NewLine;
                }
            }


            return helptext;
        }
    }
}