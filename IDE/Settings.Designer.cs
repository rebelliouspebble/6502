
using System.ComponentModel;

namespace IDE
{
    /// <summary>
    /// Class Settings.
    /// Implements the <see cref="System.Windows.Forms.Form" />
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabEditSettings = new System.Windows.Forms.TabPage();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.elementList = new System.Windows.Forms.ListBox();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.tabBuildSettings = new System.Windows.Forms.TabPage();
            this.txtCompileSettings = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkCompilerDefault = new System.Windows.Forms.CheckBox();
            this.btnBrowseCompiler = new System.Windows.Forms.Button();
            this.txtCompiler = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabEditSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabBuildSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabEditSettings);
            this.tabControl.Controls.Add(this.tabBuildSettings);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(933, 519);
            this.tabControl.TabIndex = 0;
            // 
            // tabEditSettings
            // 
            this.tabEditSettings.Controls.Add(this.splitContainer1);
            this.tabEditSettings.Location = new System.Drawing.Point(4, 24);
            this.tabEditSettings.Name = "tabEditSettings";
            this.tabEditSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabEditSettings.Size = new System.Drawing.Size(925, 491);
            this.tabEditSettings.TabIndex = 0;
            this.tabEditSettings.Text = "Editor Settings";
            this.tabEditSettings.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.elementList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.propertyGrid1);
            this.splitContainer1.Size = new System.Drawing.Size(919, 485);
            this.splitContainer1.SplitterDistance = 302;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // elementList
            // 
            this.elementList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementList.FormattingEnabled = true;
            this.elementList.ItemHeight = 15;
            this.elementList.Location = new System.Drawing.Point(0, 0);
            this.elementList.Name = "elementList";
            this.elementList.Size = new System.Drawing.Size(302, 485);
            this.elementList.TabIndex = 0;
            this.elementList.SelectedIndexChanged += new System.EventHandler(this.elementList_SelectedIndexChanged);
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(612, 485);
            this.propertyGrid1.TabIndex = 0;
            // 
            // tabBuildSettings
            // 
            this.tabBuildSettings.Controls.Add(this.txtCompileSettings);
            this.tabBuildSettings.Controls.Add(this.label2);
            this.tabBuildSettings.Controls.Add(this.chkCompilerDefault);
            this.tabBuildSettings.Controls.Add(this.btnBrowseCompiler);
            this.tabBuildSettings.Controls.Add(this.txtCompiler);
            this.tabBuildSettings.Controls.Add(this.label1);
            this.tabBuildSettings.Location = new System.Drawing.Point(4, 22);
            this.tabBuildSettings.Name = "tabBuildSettings";
            this.tabBuildSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabBuildSettings.Size = new System.Drawing.Size(925, 493);
            this.tabBuildSettings.TabIndex = 1;
            this.tabBuildSettings.Text = "Build Configuration";
            this.tabBuildSettings.UseVisualStyleBackColor = true;
            // 
            // txtCompileSettings
            // 
            this.txtCompileSettings.Enabled = false;
            this.txtCompileSettings.Location = new System.Drawing.Point(149, 117);
            this.txtCompileSettings.Name = "txtCompileSettings";
            this.txtCompileSettings.Size = new System.Drawing.Size(594, 23);
            this.txtCompileSettings.TabIndex = 5;
            this.txtCompileSettings.Text = "-lkernel.txt -f3 -v5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Compiler Settings";
            // 
            // chkCompilerDefault
            // 
            this.chkCompilerDefault.AutoSize = true;
            this.chkCompilerDefault.Checked = true;
            this.chkCompilerDefault.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkCompilerDefault.Location = new System.Drawing.Point(43, 29);
            this.chkCompilerDefault.Name = "chkCompilerDefault";
            this.chkCompilerDefault.Size = new System.Drawing.Size(143, 19);
            this.chkCompilerDefault.TabIndex = 3;
            this.chkCompilerDefault.Text = "Use Default Compiler?";
            this.chkCompilerDefault.UseVisualStyleBackColor = true;
            this.chkCompilerDefault.CheckedChanged += new System.EventHandler(this.chkCompilerDefault_CheckedChanged);
            // 
            // btnBrowseCompiler
            // 
            this.btnBrowseCompiler.Enabled = false;
            this.btnBrowseCompiler.Location = new System.Drawing.Point(785, 53);
            this.btnBrowseCompiler.Name = "btnBrowseCompiler";
            this.btnBrowseCompiler.Size = new System.Drawing.Size(87, 27);
            this.btnBrowseCompiler.TabIndex = 2;
            this.btnBrowseCompiler.Text = "Browse";
            this.btnBrowseCompiler.UseVisualStyleBackColor = true;
            this.btnBrowseCompiler.Click += new System.EventHandler(this.btnBrowseCompiler_Click);
            // 
            // txtCompiler
            // 
            this.txtCompiler.Enabled = false;
            this.txtCompiler.Location = new System.Drawing.Point(119, 55);
            this.txtCompiler.Name = "txtCompiler";
            this.txtCompiler.Size = new System.Drawing.Size(625, 23);
            this.txtCompiler.TabIndex = 1;
            this.txtCompiler.Text = "dasm.exe";
            this.txtCompiler.TextChanged += new System.EventHandler(this.TxtCompiler_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Compiler";
            // 
            // Settings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(933, 519);
            this.Controls.Add(this.tabControl);
            this.Name = "Settings";
            this.Text = "Settings";
            this.Load += new System.EventHandler(this.Settings_Load);
            this.tabControl.ResumeLayout(false);
            this.tabEditSettings.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabBuildSettings.ResumeLayout(false);
            this.tabBuildSettings.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBrowseCompiler;
        private System.Windows.Forms.CheckBox chkCompilerDefault;
        private System.Windows.Forms.PropertyGrid propertyGrid1;
        private System.Windows.Forms.ListBox elementList;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TabPage tabEditSettings;
        private System.Windows.Forms.TextBox txtCompiler;
        private System.Windows.Forms.TabPage tabBuildSettings;
        private System.Windows.Forms.TextBox txtCompileSettings;
        private System.Windows.Forms.TabControl tabControl;
    }
}