﻿/**********************************************************************************************//**
 * \file    Program.cs.
 *
 * \brief   Implements the program class
 **************************************************************************************************/

using System;
using System.Windows.Forms;

namespace IDE
{
    /**********************************************************************************************//**
     * \class   Program
     *
     * \brief   Class Program.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    internal static class Program
    {
        /**********************************************************************************************//**
         * \fn  private static void Main()
         *
         * \brief   The main entry point for the application.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}