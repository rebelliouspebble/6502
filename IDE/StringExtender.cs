/**********************************************************************************************//**
 * \file    StringExtender.cs.
 *
 * \brief   Implements the string extender class
 **************************************************************************************************/

using System;

namespace IDE
{
    /**********************************************************************************************//**
     * \class   StringExtender
     *
     * \brief   Extends the string class
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public static class StringExtender
    {
        /**********************************************************************************************//**
         * \fn  public static string TrimEndString(this string input, string suffixToRemove, StringComparison comparisonType)
         *
         * \brief   A string extension method that trims a suffix from the end of a string
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   input           The input to act on.
         * \param   suffixToRemove  The suffix to remove.
         * \param   comparisonType  Comparison type
         *
         * \returns A string.
         **************************************************************************************************/

        public static string TrimEndString(this string input, string suffixToRemove,
            StringComparison comparisonType) {

            if (input != null && suffixToRemove != null
                              && input.EndsWith(suffixToRemove, comparisonType)) {
                return input.Substring(0, input.Length - suffixToRemove.Length);
            }
            else return input;
        }
    }
}