﻿
namespace IDE
{
    /// <summary>
    /// Class ExecutionScreen.
    /// Implements the <see cref="System.Windows.Forms.Form" />
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    partial class ExecutionScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.grpInstList = new System.Windows.Forms.GroupBox();
            this.instGrid = new System.Windows.Forms.DataGridView();
            this.Instruction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AddressingMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Operand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Information = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpMem = new System.Windows.Forms.GroupBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.MemoryPageSelector = new System.Windows.Forms.ComboBox();
            this.memGrid = new System.Windows.Forms.DataGridView();
            this.col00 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col01 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col02 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col03 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col04 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col05 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col06 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col07 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col08 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col09 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0A = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0B = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0C = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0D = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0E = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col0F = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpControl = new System.Windows.Forms.GroupBox();
            this.lblSpeedBar = new System.Windows.Forms.Label();
            this.lblStepExec = new System.Windows.Forms.Label();
            this.lblStopExec = new System.Windows.Forms.Label();
            this.lblStartExec = new System.Windows.Forms.Label();
            this.speedBar = new System.Windows.Forms.TrackBar();
            this.btnStepExec = new System.Windows.Forms.Button();
            this.btnStopExec = new System.Windows.Forms.Button();
            this.btnStartExec = new System.Windows.Forms.Button();
            this.grpRegisters = new System.Windows.Forms.GroupBox();
            this.lblStatusReg = new System.Windows.Forms.Label();
            this.lblCycles = new System.Windows.Forms.Label();
            this.lblStckPntr = new System.Windows.Forms.Label();
            this.lblRegY = new System.Windows.Forms.Label();
            this.lblRegX = new System.Windows.Forms.Label();
            this.lblRegA = new System.Windows.Forms.Label();
            this.lblCrntInst = new System.Windows.Forms.Label();
            this.lblPC = new System.Windows.Forms.Label();
            this.txtSP = new System.Windows.Forms.TextBox();
            this.txtCyc = new System.Windows.Forms.TextBox();
            this.txtY = new System.Windows.Forms.TextBox();
            this.txtX = new System.Windows.Forms.TextBox();
            this.txtA = new System.Windows.Forms.TextBox();
            this.txtCI = new System.Windows.Forms.TextBox();
            this.txtPC = new System.Windows.Forms.TextBox();
            this.FlagsListBox = new System.Windows.Forms.CheckedListBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.grpInstList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.instGrid)).BeginInit();
            this.grpMem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.memGrid)).BeginInit();
            this.grpControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.speedBar)).BeginInit();
            this.grpRegisters.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(
                new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.grpInstList, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.grpMem, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.grpControl, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.grpRegisters, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(
                new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1295, 749);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // grpInstList
            // 
            this.grpInstList.Controls.Add(this.instGrid);
            this.grpInstList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpInstList.Location = new System.Drawing.Point(3, 3);
            this.grpInstList.Name = "grpInstList";
            this.grpInstList.Size = new System.Drawing.Size(641, 368);
            this.grpInstList.TabIndex = 0;
            this.grpInstList.TabStop = false;
            this.grpInstList.Text = "Instruction List";
            // 
            // instGrid
            // 
            this.instGrid.AllowUserToAddRows = false;
            this.instGrid.AllowUserToDeleteRows = false;
            this.instGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.instGrid.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.instGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[]
                {this.Instruction, this.AddressingMode, this.Operand, this.Information});
            this.instGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.instGrid.Location = new System.Drawing.Point(3, 19);
            this.instGrid.Name = "instGrid";
            this.instGrid.Size = new System.Drawing.Size(635, 346);
            this.instGrid.TabIndex = 0;
            // 
            // Instruction
            // 
            this.Instruction.HeaderText = "Instruction";
            this.Instruction.Name = "Instruction";
            // 
            // AddressingMode
            // 
            this.AddressingMode.HeaderText = "Addressing Mode";
            this.AddressingMode.Name = "AddressingMode";
            // 
            // Operand
            // 
            this.Operand.HeaderText = "Operand";
            this.Operand.Name = "Operand";
            // 
            // Information
            // 
            this.Information.HeaderText = "Information";
            this.Information.Name = "Information";
            // 
            // grpMem
            // 
            this.grpMem.Controls.Add(this.splitContainer1);
            this.grpMem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpMem.Location = new System.Drawing.Point(650, 3);
            this.grpMem.Name = "grpMem";
            this.grpMem.Size = new System.Drawing.Size(642, 368);
            this.grpMem.TabIndex = 1;
            this.grpMem.TabStop = false;
            this.grpMem.Text = "Memory Map";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(3, 19);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.MemoryPageSelector);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.memGrid);
            this.splitContainer1.Size = new System.Drawing.Size(636, 346);
            this.splitContainer1.SplitterDistance = 28;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // MemoryPageSelector
            // 
            this.MemoryPageSelector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MemoryPageSelector.FormattingEnabled = true;
            this.MemoryPageSelector.Location = new System.Drawing.Point(0, 0);
            this.MemoryPageSelector.Name = "MemoryPageSelector";
            this.MemoryPageSelector.Size = new System.Drawing.Size(636, 23);
            this.MemoryPageSelector.TabIndex = 0;
            this.MemoryPageSelector.SelectedIndexChanged +=
                new System.EventHandler(this.MemoryPageSelector_SelectedIndexChanged);
            // 
            // memGrid
            // 
            this.memGrid.AllowUserToAddRows = false;
            this.memGrid.AllowUserToDeleteRows = false;
            this.memGrid.ColumnHeadersHeightSizeMode =
                System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.memGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[]
            {
                this.col00, this.col01, this.col02, this.col03, this.col04, this.col05, this.col06, this.col07,
                this.col08, this.col09, this.col0A, this.col0B, this.col0C, this.col0D, this.col0E, this.col0F
            });
            this.memGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.memGrid.Location = new System.Drawing.Point(0, 0);
            this.memGrid.Name = "memGrid";
            this.memGrid.ReadOnly = true;
            this.memGrid.Size = new System.Drawing.Size(636, 313);
            this.memGrid.TabIndex = 0;
            // 
            // col00
            // 
            this.col00.HeaderText = "00";
            this.col00.Name = "col00";
            this.col00.ReadOnly = true;
            this.col00.Width = 30;
            // 
            // col01
            // 
            this.col01.HeaderText = "01";
            this.col01.Name = "col01";
            this.col01.ReadOnly = true;
            this.col01.Width = 30;
            // 
            // col02
            // 
            this.col02.HeaderText = "02";
            this.col02.Name = "col02";
            this.col02.ReadOnly = true;
            this.col02.Width = 30;
            // 
            // col03
            // 
            this.col03.HeaderText = "03";
            this.col03.Name = "col03";
            this.col03.ReadOnly = true;
            this.col03.Width = 30;
            // 
            // col04
            // 
            this.col04.HeaderText = "04";
            this.col04.Name = "col04";
            this.col04.ReadOnly = true;
            this.col04.Width = 30;
            // 
            // col05
            // 
            this.col05.HeaderText = "05";
            this.col05.Name = "col05";
            this.col05.ReadOnly = true;
            this.col05.Width = 30;
            // 
            // col06
            // 
            this.col06.HeaderText = "06";
            this.col06.Name = "col06";
            this.col06.ReadOnly = true;
            this.col06.Width = 30;
            // 
            // col07
            // 
            this.col07.HeaderText = "07";
            this.col07.Name = "col07";
            this.col07.ReadOnly = true;
            this.col07.Width = 30;
            // 
            // col08
            // 
            this.col08.HeaderText = "08";
            this.col08.Name = "col08";
            this.col08.ReadOnly = true;
            this.col08.Width = 30;
            // 
            // col09
            // 
            this.col09.HeaderText = "09";
            this.col09.Name = "col09";
            this.col09.ReadOnly = true;
            this.col09.Width = 30;
            // 
            // col0A
            // 
            this.col0A.HeaderText = "0A";
            this.col0A.Name = "col0A";
            this.col0A.ReadOnly = true;
            this.col0A.Width = 30;
            // 
            // col0B
            // 
            this.col0B.HeaderText = "0B";
            this.col0B.Name = "col0B";
            this.col0B.ReadOnly = true;
            this.col0B.Width = 30;
            // 
            // col0C
            // 
            this.col0C.HeaderText = "0C";
            this.col0C.Name = "col0C";
            this.col0C.ReadOnly = true;
            this.col0C.Width = 30;
            // 
            // col0D
            // 
            this.col0D.HeaderText = "0D";
            this.col0D.Name = "col0D";
            this.col0D.ReadOnly = true;
            this.col0D.Width = 30;
            // 
            // col0E
            // 
            this.col0E.HeaderText = "0E";
            this.col0E.Name = "col0E";
            this.col0E.ReadOnly = true;
            this.col0E.Width = 30;
            // 
            // col0F
            // 
            this.col0F.HeaderText = "0F";
            this.col0F.Name = "col0F";
            this.col0F.ReadOnly = true;
            this.col0F.Width = 30;
            // 
            // grpControl
            // 
            this.grpControl.Controls.Add(this.lblSpeedBar);
            this.grpControl.Controls.Add(this.lblStepExec);
            this.grpControl.Controls.Add(this.lblStopExec);
            this.grpControl.Controls.Add(this.lblStartExec);
            this.grpControl.Controls.Add(this.speedBar);
            this.grpControl.Controls.Add(this.btnStepExec);
            this.grpControl.Controls.Add(this.btnStopExec);
            this.grpControl.Controls.Add(this.btnStartExec);
            this.grpControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpControl.Location = new System.Drawing.Point(650, 377);
            this.grpControl.Name = "grpControl";
            this.grpControl.Size = new System.Drawing.Size(642, 369);
            this.grpControl.TabIndex = 2;
            this.grpControl.TabStop = false;
            this.grpControl.Text = "Controls";
            // 
            // lblSpeedBar
            // 
            this.lblSpeedBar.AutoSize = true;
            this.lblSpeedBar.Location = new System.Drawing.Point(44, 253);
            this.lblSpeedBar.Name = "lblSpeedBar";
            this.lblSpeedBar.Size = new System.Drawing.Size(39, 15);
            this.lblSpeedBar.TabIndex = 7;
            this.lblSpeedBar.Text = "Speed";
            // 
            // lblStepExec
            // 
            this.lblStepExec.AutoSize = true;
            this.lblStepExec.Location = new System.Drawing.Point(155, 197);
            this.lblStepExec.Name = "lblStepExec";
            this.lblStepExec.Size = new System.Drawing.Size(216, 15);
            this.lblStepExec.TabIndex = 6;
            this.lblStepExec.Text = "Step Execution Forward by 1 Instruction";
            // 
            // lblStopExec
            // 
            this.lblStopExec.AutoSize = true;
            this.lblStopExec.Location = new System.Drawing.Point(155, 130);
            this.lblStopExec.Name = "lblStopExec";
            this.lblStopExec.Size = new System.Drawing.Size(122, 15);
            this.lblStopExec.TabIndex = 5;
            this.lblStopExec.Text = "Stop/Pause Execution";
            // 
            // lblStartExec
            // 
            this.lblStartExec.AutoSize = true;
            this.lblStartExec.Location = new System.Drawing.Point(155, 65);
            this.lblStartExec.Name = "lblStartExec";
            this.lblStartExec.Size = new System.Drawing.Size(127, 15);
            this.lblStartExec.TabIndex = 4;
            this.lblStartExec.Text = "Start/Restart Execution";
            // 
            // speedBar
            // 
            this.speedBar.Location = new System.Drawing.Point(48, 271);
            this.speedBar.Maximum = 20;
            this.speedBar.Minimum = 1;
            this.speedBar.Name = "speedBar";
            this.speedBar.Size = new System.Drawing.Size(502, 45);
            this.speedBar.TabIndex = 3;
            this.speedBar.Value = 1;
            this.speedBar.Scroll += new System.EventHandler(this.speedBar_Scroll);
            // 
            // btnStepExec
            // 
            this.btnStepExec.Location = new System.Drawing.Point(48, 192);
            this.btnStepExec.Name = "btnStepExec";
            this.btnStepExec.Size = new System.Drawing.Size(87, 27);
            this.btnStepExec.TabIndex = 2;
            this.btnStepExec.Text = "Step";
            this.btnStepExec.UseVisualStyleBackColor = true;
            this.btnStepExec.Click += new System.EventHandler(this.btnStepExec_Click);
            // 
            // btnStopExec
            // 
            this.btnStopExec.Location = new System.Drawing.Point(48, 125);
            this.btnStopExec.Name = "btnStopExec";
            this.btnStopExec.Size = new System.Drawing.Size(87, 27);
            this.btnStopExec.TabIndex = 1;
            this.btnStopExec.Text = "Stop";
            this.btnStopExec.UseVisualStyleBackColor = true;
            this.btnStopExec.Click += new System.EventHandler(this.btnStopExec_Click);
            // 
            // btnStartExec
            // 
            this.btnStartExec.Location = new System.Drawing.Point(48, 59);
            this.btnStartExec.Name = "btnStartExec";
            this.btnStartExec.Size = new System.Drawing.Size(87, 27);
            this.btnStartExec.TabIndex = 0;
            this.btnStartExec.Text = "Start";
            this.btnStartExec.UseVisualStyleBackColor = true;
            this.btnStartExec.Click += new System.EventHandler(this.btnStartExec_Click);
            // 
            // grpRegisters
            // 
            this.grpRegisters.Controls.Add(this.lblStatusReg);
            this.grpRegisters.Controls.Add(this.lblCycles);
            this.grpRegisters.Controls.Add(this.lblStckPntr);
            this.grpRegisters.Controls.Add(this.lblRegY);
            this.grpRegisters.Controls.Add(this.lblRegX);
            this.grpRegisters.Controls.Add(this.lblRegA);
            this.grpRegisters.Controls.Add(this.lblCrntInst);
            this.grpRegisters.Controls.Add(this.lblPC);
            this.grpRegisters.Controls.Add(this.txtSP);
            this.grpRegisters.Controls.Add(this.txtCyc);
            this.grpRegisters.Controls.Add(this.txtY);
            this.grpRegisters.Controls.Add(this.txtX);
            this.grpRegisters.Controls.Add(this.txtA);
            this.grpRegisters.Controls.Add(this.txtCI);
            this.grpRegisters.Controls.Add(this.txtPC);
            this.grpRegisters.Controls.Add(this.FlagsListBox);
            this.grpRegisters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpRegisters.Location = new System.Drawing.Point(3, 377);
            this.grpRegisters.Name = "grpRegisters";
            this.grpRegisters.Size = new System.Drawing.Size(641, 369);
            this.grpRegisters.TabIndex = 3;
            this.grpRegisters.TabStop = false;
            this.grpRegisters.Text = "Registers";
            // 
            // lblStatusReg
            // 
            this.lblStatusReg.AutoSize = true;
            this.lblStatusReg.Location = new System.Drawing.Point(10, 55);
            this.lblStatusReg.Name = "lblStatusReg";
            this.lblStatusReg.Size = new System.Drawing.Size(89, 15);
            this.lblStatusReg.TabIndex = 15;
            this.lblStatusReg.Text = "Status Registers";
            // 
            // lblCycles
            // 
            this.lblCycles.AutoSize = true;
            this.lblCycles.Location = new System.Drawing.Point(173, 207);
            this.lblCycles.Name = "lblCycles";
            this.lblCycles.Size = new System.Drawing.Size(72, 15);
            this.lblCycles.TabIndex = 14;
            this.lblCycles.Text = "Cycle Count";
            // 
            // lblStckPntr
            // 
            this.lblStckPntr.AutoSize = true;
            this.lblStckPntr.Location = new System.Drawing.Point(486, 120);
            this.lblStckPntr.Name = "lblStckPntr";
            this.lblStckPntr.Size = new System.Drawing.Size(76, 15);
            this.lblStckPntr.TabIndex = 13;
            this.lblStckPntr.Text = "Stack Pointer";
            // 
            // lblRegY
            // 
            this.lblRegY.AutoSize = true;
            this.lblRegY.Location = new System.Drawing.Point(383, 120);
            this.lblRegY.Name = "lblRegY";
            this.lblRegY.Size = new System.Drawing.Size(59, 15);
            this.lblRegY.TabIndex = 12;
            this.lblRegY.Text = "Y Register";
            // 
            // lblRegX
            // 
            this.lblRegX.AutoSize = true;
            this.lblRegX.Location = new System.Drawing.Point(285, 120);
            this.lblRegX.Name = "lblRegX";
            this.lblRegX.Size = new System.Drawing.Size(59, 15);
            this.lblRegX.TabIndex = 11;
            this.lblRegX.Text = "X Register";
            // 
            // lblRegA
            // 
            this.lblRegA.AutoSize = true;
            this.lblRegA.Location = new System.Drawing.Point(173, 120);
            this.lblRegA.Name = "lblRegA";
            this.lblRegA.Size = new System.Drawing.Size(76, 15);
            this.lblRegA.TabIndex = 10;
            this.lblRegA.Text = "Accumulator";
            // 
            // lblCrntInst
            // 
            this.lblCrntInst.AutoSize = true;
            this.lblCrntInst.Location = new System.Drawing.Point(402, 55);
            this.lblCrntInst.Name = "lblCrntInst";
            this.lblCrntInst.Size = new System.Drawing.Size(107, 15);
            this.lblCrntInst.TabIndex = 9;
            this.lblCrntInst.Text = "Current Instruction";
            // 
            // lblPC
            // 
            this.lblPC.AutoSize = true;
            this.lblPC.Location = new System.Drawing.Point(173, 55);
            this.lblPC.Name = "lblPC";
            this.lblPC.Size = new System.Drawing.Size(99, 15);
            this.lblPC.TabIndex = 8;
            this.lblPC.Text = "Program Counter";
            // 
            // txtSP
            // 
            this.txtSP.Location = new System.Drawing.Point(490, 138);
            this.txtSP.Name = "txtSP";
            this.txtSP.Size = new System.Drawing.Size(79, 23);
            this.txtSP.TabIndex = 7;
            // 
            // txtCyc
            // 
            this.txtCyc.Enabled = false;
            this.txtCyc.Location = new System.Drawing.Point(176, 225);
            this.txtCyc.Name = "txtCyc";
            this.txtCyc.Size = new System.Drawing.Size(116, 23);
            this.txtCyc.TabIndex = 6;
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(386, 138);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(68, 23);
            this.txtY.TabIndex = 5;
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(288, 138);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(67, 23);
            this.txtX.TabIndex = 4;
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(176, 138);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(72, 23);
            this.txtA.TabIndex = 3;
            // 
            // txtCI
            // 
            this.txtCI.Location = new System.Drawing.Point(406, 74);
            this.txtCI.Name = "txtCI";
            this.txtCI.Size = new System.Drawing.Size(163, 23);
            this.txtCI.TabIndex = 2;
            // 
            // txtPC
            // 
            this.txtPC.Location = new System.Drawing.Point(176, 74);
            this.txtPC.Name = "txtPC";
            this.txtPC.Size = new System.Drawing.Size(157, 23);
            this.txtPC.TabIndex = 1;
            // 
            // FlagsListBox
            // 
            this.FlagsListBox.ColumnWidth = 3;
            this.FlagsListBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F,
                System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.FlagsListBox.FormattingEnabled = true;
            this.FlagsListBox.Items.AddRange(new object[] {"N", "V", "B", "D", "I", "Z", "C"});
            this.FlagsListBox.Location = new System.Drawing.Point(7, 74);
            this.FlagsListBox.Name = "FlagsListBox";
            this.FlagsListBox.Size = new System.Drawing.Size(139, 109);
            this.FlagsListBox.TabIndex = 0;
            // 
            // ExecutionScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1295, 749);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "ExecutionScreen";
            this.Text = "ExecutionScreen";
            this.Load += new System.EventHandler(this.ExecutionScreen_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.grpInstList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.instGrid)).EndInit();
            this.grpMem.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.memGrid)).EndInit();
            this.grpControl.ResumeLayout(false);
            this.grpControl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize) (this.speedBar)).EndInit();
            this.grpRegisters.ResumeLayout(false);
            this.grpRegisters.PerformLayout();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label lblPC;
        private System.Windows.Forms.Label lblCrntInst;
        private System.Windows.Forms.Label lblRegA;
        private System.Windows.Forms.Label lblRegX;
        private System.Windows.Forms.Label lblRegY;
        private System.Windows.Forms.Label lblStckPntr;
        private System.Windows.Forms.Label lblCycles;
        private System.Windows.Forms.Label lblStatusReg;
        private System.Windows.Forms.Label lblStartExec;
        private System.Windows.Forms.Label lblStopExec;
        private System.Windows.Forms.Label lblStepExec;
        private System.Windows.Forms.Label lblSpeedBar;
        private System.Windows.Forms.TextBox txtPC;
        private System.Windows.Forms.TextBox txtCI;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.TextBox txtCyc;
        private System.Windows.Forms.TextBox txtSP;
        private System.Windows.Forms.DataGridView instGrid;
        private System.Windows.Forms.CheckedListBox FlagsListBox;
        private System.Windows.Forms.Button btnStartExec;
        private System.Windows.Forms.Button btnStopExec;
        private System.Windows.Forms.Button btnStepExec;
        private System.Windows.Forms.TrackBar speedBar;
        private System.Windows.Forms.DataGridView memGrid;
        private System.Windows.Forms.ComboBox MemoryPageSelector;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox grpRegisters;
        private System.Windows.Forms.GroupBox grpControl;
        private System.Windows.Forms.GroupBox grpMem;
        private System.Windows.Forms.GroupBox grpInstList;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col00;
        private System.Windows.Forms.DataGridViewTextBoxColumn col01;
        private System.Windows.Forms.DataGridViewTextBoxColumn col02;
        private System.Windows.Forms.DataGridViewTextBoxColumn col03;
        private System.Windows.Forms.DataGridViewTextBoxColumn col04;
        private System.Windows.Forms.DataGridViewTextBoxColumn col05;
        private System.Windows.Forms.DataGridViewTextBoxColumn col06;
        private System.Windows.Forms.DataGridViewTextBoxColumn col07;
        private System.Windows.Forms.DataGridViewTextBoxColumn col08;
        private System.Windows.Forms.DataGridViewTextBoxColumn col09;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0A;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0B;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0C;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0D;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0E;
        private System.Windows.Forms.DataGridViewTextBoxColumn col0F;
        private System.Windows.Forms.DataGridViewTextBoxColumn Information;
        private System.Windows.Forms.DataGridViewTextBoxColumn Operand;
        private System.Windows.Forms.DataGridViewTextBoxColumn Instruction;
        private System.Windows.Forms.DataGridViewTextBoxColumn AddressingMode;
    }
}