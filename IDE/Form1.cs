﻿

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ScintillaNET;

namespace IDE
{
    /**********************************************************************************************/
    /**
     * \class   MainForm
     *
     * \brief   Class MainForm. Implements the <see cref="System.Windows.Forms.Form" />
     *
     * \author  Glenn
     * \date    10/01/2020
     *
     * \sa  System.Windows.Forms.Form
     **************************************************************************************************/
    public partial class MainForm : Form
    {
        /** \brief   Flag for if file has just been saved */
        private bool JustSaved;

        /** \brief   Flag for if file is new */
        private bool NotNewFile;

        /** \brief   Settings form */
        private Settings settings;

        /** \brief  The current compiled binary file*/
        private string currentBinary;

        /**********************************************************************************************/
        /**
         * \fn  public MainForm()
         *
         * \brief   Initializes a new instance of the <see cref="MainForm"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public MainForm()
        {

            InitializeComponent();
            txtScintilla.SetEditorStyle("Consolas", 12, false);
            initControls();
            Debug.Print(txtScintilla.DescribeKeywordSets());
            Debug.Print("lmao");
            Text = "6502 IDE - untitled.asm";
            JustSaved = true;
            NotNewFile = false;
            settings = new Settings(txtScintilla);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void initControls()
         *
         * \brief   Initializes the controls.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void initControls()
        {
            //Init SaveDialog
            SaveDialog.CreatePrompt = false;
            SaveDialog.OverwritePrompt = true;
            SaveDialog.AddExtension = true;
            SaveDialog.DefaultExt = "asm";
            SaveDialog.FileName = "untitled";
            SaveDialog.Title = "Save File";

            //Init OpenDialog
            OpenDialog.Multiselect = false;
            OpenDialog.CheckFileExists = true;
            OpenDialog.FileName = "";
        }

        /**********************************************************************************************/
        /**
         * \fn  private void MenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
         *
         * \brief   Handles the ItemClicked event of the MenuStrip control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="ToolStripItemClickedEventArgs"/> instance containing the event
         *                  data.
         **************************************************************************************************/
        private void MenuStrip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
        }

        /**********************************************************************************************/
        /**
         * \fn  private void txtScintilla_CharAdded(object sender, CharAddedEventArgs e)
         *
         * \brief   Handles the CharAdded event of the txtScintilla control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="CharAddedEventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void txtScintilla_CharAdded(object sender, CharAddedEventArgs e)
        {
            txtScintilla.SetColumnMargins();
            txtScintilla.ShowAutoComplete();
            txtScintilla.ShowToolTip();
            if (JustSaved) JustSaved = false;
            if (txtScintilla.Text == "") JustSaved = true;
            if (!JustSaved & !Text.EndsWith("*")) Text = Text + "*";
        }

        /**********************************************************************************************/
        /**
         * \fn  private void saveToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the saveToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFile();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void OpenFile()
         *
         * \brief   Opens a file.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void OpenFile()
        {
            OpenDialog.ShowDialog();
            var file = OpenDialog.OpenFile();
            string content;
            using (var streamReader = new StreamReader(file, Encoding.UTF8))
            {
                content = streamReader.ReadToEnd();
            }

            txtScintilla.Text = content;
            Text = "6502 IDE - " + OpenDialog.FileName;
            SaveDialog.FileName = OpenDialog.FileName;
            NotNewFile = true;
            file.Close();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void SaveFile()
         *
         * \brief   Saves the currently open file
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void SaveFile()
        {
            Stream file;
            if (!NotNewFile) SaveDialog.ShowDialog();

            file = SaveDialog.OpenFile();
            var info = new UTF8Encoding(true).GetBytes(txtScintilla.Text);
            file.Write(info, 0, info.Length);
            Text = "6502 IDE - " + SaveDialog.FileName;
            JustSaved = true;
            NotNewFile = true;
            if (Text.EndsWith("*")) Text = Text.TrimEnd('*');
            file.Close();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void NewFile()
         *
         * \brief   Creates a new file.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void NewFile()
        {
            if (!JustSaved)
            {
                var dialogResult = MessageBox.Show("This will delete all work. Do you want to save first?",
                    "Warning", MessageBoxButtons.YesNoCancel);
                if (dialogResult == DialogResult.Yes)
                {
                    SaveFile();
                    txtScintilla.Text = "";
                    Text = "6502 IDE - untitled.asm";
                }

                else if (dialogResult == DialogResult.No)
                {
                    txtScintilla.Text = "";
                    Text = "6502 IDE - untitled.asm";
                    JustSaved = true;
                }

                else if (dialogResult == DialogResult.Cancel)
                {
                }
            }

            else
            {
                txtScintilla.Text = "";
                Text = "6502 IDE - untitled.asm";
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void openToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the openToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFile();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void newToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the newToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            NewFile();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void exitToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the exitToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        /**********************************************************************************************/
        /**
         * \fn  protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
         *
         * \brief   Processes a command key.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param [in,out]  msg     A <see cref="T:System.Windows.Forms.Message" />, passed by reference,
         *                          that represents the Win32 message to process.
         * \param           keyData One of the <see cref="T:System.Windows.Forms.Keys" /> values that
         *                          represents the key to process.
         *
         * \returns <see langword="true" /> if the keystroke was processed and consumed by the control;
         *          otherwise, <see langword="false" /> to allow further processing.
         **************************************************************************************************/
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == (Keys.Control | Keys.F))
            {
                MessageBox.Show("What the Ctrl+F?");
                return true;
            }

            if (keyData == (Keys.Control | Keys.S))
            {
                SaveFile();
                return true;
            }

            if (keyData == (Keys.Control | Keys.N))
            {
                NewFile();
                return true;
            }

            if (keyData == (Keys.Control | Keys.O))
            {
                OpenFile();
                return true;
            }


            return base.ProcessCmdKey(ref msg, keyData);
        }

        /**********************************************************************************************/
        /**
         * \fn  private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the settingsToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            settings.Show();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void copyToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the copyToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ctrl = ActiveControl;

            if (ctrl != null)

            {
                if (ctrl is TextBox)

                {
                    var tx = (TextBox) ctrl;

                    tx.Copy();
                }

                if (ctrl is Scintilla)
                {
                    var sc = (Scintilla) ctrl;
                    sc.Copy();
                }
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void cutToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the cutToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ctrl = ActiveControl;

            if (ctrl != null)

            {
                if (ctrl is TextBox)

                {
                    var tx = (TextBox) ctrl;

                    tx.Cut();
                }

                if (ctrl is Scintilla)
                {
                    var sc = (Scintilla) ctrl;
                    sc.Cut();
                }
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the pasteToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ctrl = ActiveControl;

            if (ctrl != null)

            {
                if (ctrl is TextBox)

                {
                    var tx = (TextBox) ctrl;

                    tx.Paste();
                }

                if (ctrl is Scintilla)
                {
                    var sc = (Scintilla) ctrl;
                    sc.Paste();
                }
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void undoToolStripMenuItem_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the undoToolStripMenuItem control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ctrl = ActiveControl;

            if (ctrl != null)

            {
                if (ctrl is TextBox)

                {
                    var tx = (TextBox) ctrl;

                    tx.Undo();
                }

                if (ctrl is Scintilla)
                {
                    var sc = (Scintilla) ctrl;
                    sc.Undo();
                }
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  private void BuildBinary()
         *
         * \brief   Builds a binary file from the open file.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        private void BuildBinary()
        {
            var file = SaveDialog.FileName;

            string command =
                settings.getCompilerExecutable() + " " +
                file + " " + settings.getCompilerSettings() + " -o" +
                file.TrimEndString(".asm", StringComparison.Ordinal) + ".bin";

            Debug.Print(command);

            // create the ProcessStartInfo using "cmd" as the program to be run,
            // and "/c " as the parameters.

            System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);

            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            procStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();
            // Get the output into a string
            string result = proc.StandardOutput.ReadToEnd();
            // Display the command output.
            MessageBox.Show(result);
            Debug.Print("result:");
            Debug.Print(result);

            currentBinary = file.TrimEndString(".asm", StringComparison.Ordinal) + ".bin";

        }

        private void Run()
        {
            ExecutionScreen exsc = new ExecutionScreen(currentBinary, 0x400, 0x400);
            exsc.Show();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void ToolStripRun_Click(object sender, EventArgs e)
         *
         * \brief   Handles the Click event of the ToolStripRun control.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs"/> instance containing the event data.
         **************************************************************************************************/
        private void ToolStripRun_Click(object sender, EventArgs e)
        {
            BuildBinary();
            Run();
        }

        /**********************************************************************************************/
        /**
         * \fn  private void toolStripCompile_Click(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by toolStripCompile for click events
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/
        private void toolStripCompile_Click(object sender, EventArgs e)
        {
            BuildBinary();
        }


        private void compileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildBinary();
        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuildBinary();
            Run();
        }
    }

}