/**********************************************************************************************//**
 * \file    Settings.cs.
 *
 * \brief   Implements the settings class
 **************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using ScintillaNET;

namespace IDE
{
    /**********************************************************************************************//**
     * \class   Settings
     *
     * \brief   Class Settings. Implements the <see cref="System.Windows.Forms.Form" />
     *
     * \author  Glenn
     * \date    02/02/2020
     *
     * \sa  System.Windows.Forms.Form
     **************************************************************************************************/

    public partial class Settings : Form
    {
        /** \brief   /** \brief   The scintilla */
        private readonly Scintilla scintilla;

        /**********************************************************************************************//**
         * \fn  public Settings(Scintilla scintilla)
         *
         * \brief   Initializes a new instance of the <see cref="Settings" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   scintilla   The scintilla.
         **************************************************************************************************/

        public Settings(Scintilla scintilla)
        {
            InitializeComponent();
            this.scintilla = scintilla;
        }

        /**********************************************************************************************//**
         * \fn  private void Settings_Load(object sender, EventArgs e)
         *
         * \brief   Handles the Load event of the Settings control.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs" /> instance containing the event data.
         **************************************************************************************************/

        private void Settings_Load(object sender, EventArgs e)
        {
            LoadLexerSettings();
        }

        /**********************************************************************************************//**
         * \fn  private void LoadLexerSettings()
         *
         * \brief   Loads the lexer settings.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        private void LoadLexerSettings()
        {
            elementList.Items.Clear();

            var lexerType = Type.GetType(typeof(Style).AssemblyQualifiedName.Replace(".Style", ".Style+" + Lexer.Asm));

            if (lexerType != null)
            {
                elementList.Items
                    .AddRange(
                        this.GetLexerStyles(scintilla.Lexer)
                            .Select(m => m.Key + ": " + m.Value)
                            .ToArray()
                    );
            }

            propertyGrid1.SelectedObject = null;

        }

        /**********************************************************************************************//**
         * \fn  protected Dictionary<int, string> GetLexerStyles(Lexer lexer)
         *
         * \brief   Gets the lexer styles.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   lexer   The lexer.
         *
         * \returns Dictionary&lt;System.Int32, System.String&gt;.
         **************************************************************************************************/

        protected Dictionary<int, string> GetLexerStyles(Lexer lexer)
        {
            var lexerName = Enum.GetName(typeof(Lexer), lexer);
            var lexerType = Type.GetType(typeof(Style).AssemblyQualifiedName.Replace(".Style", ".Style+" + lexerName));
            return lexerType == null
                ? new Dictionary<int, string>()
                : lexerType
                    .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                    .Where(fi => fi.IsLiteral && !fi.IsInitOnly)
                    .ToDictionary(
                        fi => (int)fi.GetRawConstantValue(),
                        fi => fi.Name
                    );
        }

        /**********************************************************************************************//**
         * \fn  private void elementList_SelectedIndexChanged(object sender, EventArgs e)
         *
         * \brief   Handles the SelectedIndexChanged event of the elementList control.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs" /> instance containing the event data.
         **************************************************************************************************/

        private void elementList_SelectedIndexChanged(object sender, EventArgs e)
        {
            var pos = int.Parse(elementList.Text.Split(':')[0]);
            propertyGrid1.SelectedObject = scintilla.Styles[pos];
        }

        /**********************************************************************************************//**
         * \fn  private void TxtCompiler_TextChanged(object sender, EventArgs e)
         *
         * \brief   Handles the TextChanged event of the TxtCompiler control.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   sender  The source of the event.
         * \param   e       The <see cref="EventArgs" /> instance containing the event data.
         **************************************************************************************************/

        private void TxtCompiler_TextChanged(object sender, EventArgs e)
        {
        }

        /**********************************************************************************************//**
         * \fn  public string getCompilerExecutable()
         *
         * \brief   Gets compiler executable
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \returns The compiler executable.
         **************************************************************************************************/

        public string getCompilerExecutable()
        {
            return txtCompiler.Text;
        }

        /**********************************************************************************************//**
         * \fn  public string getCompilerSettings()
         *
         * \brief   Gets compiler settings / flags
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \returns The compiler settings.
         **************************************************************************************************/

        public string getCompilerSettings()
        {
            return txtCompileSettings.Text;
        }

        /**********************************************************************************************//**
         * \fn  private void chkCompilerDefault_CheckedChanged(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by chkCompilerDefault for checked changed events
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/

        private void chkCompilerDefault_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCompilerDefault.Checked)
            {
                txtCompiler.Enabled = false;
                btnBrowseCompiler.Enabled = false;
            }
            else
            {
                txtCompiler.Enabled = true;
                btnBrowseCompiler.Enabled = true;
            }
        }

        /**********************************************************************************************//**
         * \fn  private void btnBrowseCompiler_Click(object sender, EventArgs e)
         *
         * \brief   Event handler. Called by btnBrowseCompiler for click events
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   sender  The source of the event.
         * \param   e       Event information.
         **************************************************************************************************/

        private void btnBrowseCompiler_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            fileDialog.InitialDirectory = "c:\\" ;
            fileDialog.Filter = "Executable Files (*.exe)|*.exe" ;
            fileDialog.FilterIndex = 0;
            fileDialog.RestoreDirectory = true ;

            if(fileDialog.ShowDialog() == DialogResult.OK)
            {
                txtCompiler.Text = fileDialog.FileName;
            }
        }
    }
}