﻿/**********************************************************************************************//**
 * \file    Program.cs.
 *
 * \brief   Implements the program class
 **************************************************************************************************/

using System;
using System.Collections;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using SystemEmulator;
using Console = Colorful.Console;

namespace ConsoleInterpreter
{
    /**********************************************************************************************//**
     * \class   Program
     *
     * \brief   ConsoleInterpreter Program Class
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    internal class Program
    {
        /**********************************************************************************************//**
         * \fn  public static void Main(string[] args)
         *
         * \brief   Defines the entry point of the application.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   args    The arguments.
         **************************************************************************************************/

        public static void Main(string[] args)
        {
            var processor = new Processor(300, new Memory());
            processor.setupTimer();

            foreach (var i in Enum.GetValues(typeof(InstructionEnum))) Debug.Write(i + " ");
            while (true)
            {
                Console.Write("6502> ", Color.CornflowerBlue);
                var command = Console.ReadLine();
                var instruction = new InstructionDetails();


                if ((command != "help") & (command != "status") & (command != "start") & !command.StartsWith("load") &
                    (command != "stop") & !command.StartsWith("memory"))
                {
                    var splitted = command.Split(' ');
                    instruction.instruction = Utility.GetEnumValue<InstructionEnum>(splitted[0]);
                    if (splitted.Length == 2)
                    {
                        if (splitted[1] == "A")
                        {
                            instruction.addressingmode = CPUAddressingMode.Accumulator;
                            instruction.operand = 0;
                        }
                        else if (splitted[1][0] == '#')
                        {
                            instruction.addressingmode = CPUAddressingMode.Immediate;
                            instruction.operand = short.Parse(splitted[1].Substring(1), NumberStyles.HexNumber);
                        }
                        else if ((splitted[1][0] == '$') & ((splitted[1].Length == 3) | (splitted[1][3] == ',')))
                        {
                            instruction.operand = short.Parse(splitted[1].Substring(1, 2), NumberStyles.HexNumber);
                            if (splitted[1].EndsWith(",X"))
                                instruction.addressingmode = CPUAddressingMode.ZeroPageX;
                            else if (splitted[1].EndsWith(",Y"))
                                instruction.addressingmode = CPUAddressingMode.ZeroPageY;
                            else
                                instruction.addressingmode = CPUAddressingMode.ZeroPage;
                        }
                        else if ((splitted[1][0] == '$') & (splitted[1].Length >= 5))
                        {
                            instruction.operand = short.Parse(splitted[1].Substring(1, 4), NumberStyles.HexNumber);
                            if (splitted[1].EndsWith(",X"))
                                instruction.addressingmode = CPUAddressingMode.AbsoluteX;
                            else if (splitted[1].EndsWith(",Y"))
                                instruction.addressingmode = CPUAddressingMode.AbsoluteY;
                            else
                                instruction.addressingmode = CPUAddressingMode.Absolute;
                        }
                        else if (splitted[1].StartsWith("("))
                        {
                            if (splitted[1].EndsWith(",X)"))
                            {
                                instruction.operand = short.Parse(splitted[1].Substring(2, 2), NumberStyles.HexNumber);
                                instruction.addressingmode = CPUAddressingMode.IndirectX;
                            }
                            else if (splitted[1].EndsWith("),Y"))
                            {
                                instruction.operand = short.Parse(splitted[1].Substring(2, 2), NumberStyles.HexNumber);
                                instruction.addressingmode = CPUAddressingMode.IndirectY;
                            }
                            else
                            {
                                instruction.operand = short.Parse(splitted[1].Substring(2, 4), NumberStyles.HexNumber);
                                instruction.addressingmode = CPUAddressingMode.Indirect;
                            }
                        }
                        else
                        {
                            instruction.addressingmode = CPUAddressingMode.Relative;
                            instruction.operand = short.Parse(splitted[1]);
                        }
                    }
                    else
                    {
                        instruction.addressingmode = CPUAddressingMode.Implicit;
                    }

                    processor.RunSingleInstruction(instruction.instruction, instruction.addressingmode,
                        instruction.operand);
                }
                else
                {
                    if (command == "help")
                    {
                        Console.WriteLine("   6502 Interpreter Help   ", Color.Chocolate);
                        Console.WriteLine("---------------------------", Color.Chocolate);
                        Console.Write("help - ", Color.Orange);
                        Console.WriteLine("shows this help page");
                        Console.Write("load - ", Color.Orange);
                        Console.WriteLine("load a binary file into memory");
                        Console.Write("status - ", Color.Orange);
                        Console.WriteLine("display processor status");
                        Console.Write("run - ", Color.Orange);
                        Console.WriteLine("start execution from memory");
                    }
                    else if (command == "status")
                    {
                        var statusreg = processor.statusToByte();
                        var BA = new BitArray(new[] {statusreg});
                        Console.WriteLine("   6502 Interpreter Status   ", Color.Chocolate);
                        Console.WriteLine("---------------------------", Color.Chocolate);
                        Console.Write("Accumulator - ", Color.Orange);
                        Console.WriteLine(processor.Accumulator);
                        Console.Write("X Register - ", Color.Orange);
                        Console.WriteLine(processor.IndexRegisterX);
                        Console.Write("Y Register - ", Color.Orange);
                        Console.WriteLine(processor.IndexRegisterY);
                        Console.WriteLine("Flags - ", Color.Orange);
                        Console.WriteLine("N | V | B | D | I | Z | C");
                        Console.WriteLine(BA[0] + "|" + BA[1] + "|" + BA[3] + "|" + BA[4] + "|" + BA[5] + "|" + BA[6] +
                                          "|" + BA[7]);
                        Console.Write("Cycles - ", Color.Orange);
                        Console.WriteLine(processor.cyclecount);
                        Console.Write("Program Counter - ", Color.Orange);
                        Console.WriteLine(processor.ProgramCounter);
                    }
                    else if (command.StartsWith("load"))
                    {
                        var splitted = command.Split(' ');
                        processor.LoadProgram(splitted[1], int.Parse(splitted[2], NumberStyles.HexNumber),
                            int.Parse(splitted[3], NumberStyles.HexNumber));
                    }
                    else if (command == "start")
                    {
                        processor.cpuclock.Start();
                    }

                    else if (command == "stop")
                    {
                        processor.cpuclock.Stop();
                    }
                    else if (command.StartsWith("memory"))
                    {
                        var splitted = command.Split(' ');
                        Console.WriteLine(
                            processor.Memory.DirectReadMemory(int.Parse(splitted[1], NumberStyles.HexNumber)));
                    }
                }
            }
        }

        /**********************************************************************************************//**
         * \struct  InstructionDetails
         *
         * \brief   Struct InstructionDetails
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        private struct InstructionDetails
        {
            /** \brief   /** \brief   The addressingmode */
            public CPUAddressingMode addressingmode;

            /** \brief   /** \brief   The instruction */
            public InstructionEnum instruction;

            /** \brief   /** \brief   The operand */
            public short operand;
        }
    }
}