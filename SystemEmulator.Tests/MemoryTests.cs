// ***********************************************************************
// Assembly         : SystemEmulator.Tests
// Author           : glenn
// Created          : 09-10-2019
//
// Last Modified By : glenn
// Last Modified On : 09-10-2019
// ***********************************************************************
// <copyright file="MemoryTests.cs" company="SystemEmulator.Tests">
//     Copyright (c) HP Inc.. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************

using System;
using System.IO;
using SystemEmulator;
using Xunit;
using Xunit.Abstractions;

namespace MemoryTests
{
    /**********************************************************************************************/
    /**
     * \class   MemoryTestFixture
     *
     * \brief   Class MemoryTestFixture.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    public class MemoryTestFixture
    {
        /** \brief   The memory */
        public Memory mem;

        /** \brief   The memory */
        public byte[] memory = File.ReadAllBytes("memory.bin");

        /** \brief   The random */
        public Random random;

        /**********************************************************************************************/
        /**
         * \fn  public MemoryTestFixture()
         *
         * \brief   Initializes a new instance of the <see cref="MemoryTestFixture"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public MemoryTestFixture()
        {
            mem = new Memory();
            random = new Random();
            mem.overwriteMemory(memory);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   MemoryTestCollection
     *
     * \brief   Class MemoryTestCollection. Implements the
     *          <see cref="Xunit.ICollectionFixture{MemoryTests.MemoryTestFixture}" />
     *
     * \author  Glenn
     * \date    10/01/2020
     *
     * \sa  Xunit.ICollectionFixture{MemoryTests.MemoryTestFixture} 
     **************************************************************************************************/
    [CollectionDefinition("MemoryCollection")]
    public class MemoryTestCollection : ICollectionFixture<MemoryTestFixture>
    {
    }

    /**********************************************************************************************/
    /**
     * \class   MemoryArrayTests
     *
     * \brief   Class MemoryArrayTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class MemoryArrayTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public MemoryArrayTests(MemoryTestFixture fixture, ITestOutputHelper output)
         *
         * \brief   Initializes a new instance of the <see cref="MemoryArrayTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         * \param   output  The output.
         **************************************************************************************************/
        public MemoryArrayTests(MemoryTestFixture fixture, ITestOutputHelper output)
        {
            Fixture = fixture;
            this.output = output;
        }

        /** \brief   The fixture */
        private MemoryTestFixture Fixture;

        /** \brief   The output */
        private readonly ITestOutputHelper output;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckMemoryLocationAcceptsStandardAddress()
         *
         * \brief   Defines the test method CheckMemoryLocationAcceptsStandardAddress.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckMemoryLocationAcceptsStandardAddress()
        {
            var memloc = new Memory.memoryLocation();
            memloc.location = 0x94F2;

            output.WriteLine(Convert.ToString(memloc.byteLow));
            output.WriteLine(Convert.ToString(memloc.byteHigh));
            Assert.Equal(0xF2, memloc.byteLow);
            Assert.Equal(0x94, memloc.byteHigh);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckMemoryLocationAcceptsZeroPageMax()
         *
         * \brief   Defines the test method CheckMemoryLocationAcceptsZeroPageMax.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckMemoryLocationAcceptsZeroPageMax()
        {
            var memloc = new Memory.memoryLocation();
            memloc.location = 0xFF;
            Assert.Equal(0xFF, memloc.byteLow);
            Assert.Equal(0x00, memloc.byteHigh);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   AccumulatorTests
     *
     * \brief   Class AccumulatorTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class AccumulatorTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public AccumulatorTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="AccumulatorTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public AccumulatorTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAccumulatorThrowsNotSupportedException()
         *
         * \brief   Defines the test method CheckAccumulatorThrowsNotSupportedException.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAccumulatorThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => Fixture.mem.ReadMem(35, CPUAddressingMode.Accumulator, 0, 0));
        }
    }

    /**********************************************************************************************/
    /**
     * \class   ImmediateTests
     *
     * \brief   Class ImmediateTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class ImmediateTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public ImmediateTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="ImmediateTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public ImmediateTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckImmediateThrowsNotSupportedException()
         *
         * \brief   Defines the test method CheckImmediateThrowsNotSupportedException.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckImmediateThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => Fixture.mem.ReadMem(35, CPUAddressingMode.Immediate, 0, 0));
        }
    }

    /**********************************************************************************************/
    /**
     * \class   ImplicitTests
     *
     * \brief   Class ImplicitTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class ImplicitTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public ImplicitTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="ImplicitTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public ImplicitTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckImplicitThrowsNotSupportedException()
         *
         * \brief   Defines the test method CheckImplicitThrowsNotSupportedException.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckImplicitThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => Fixture.mem.ReadMem(35, CPUAddressingMode.Implicit, 0, 0));
        }
    }

    /**********************************************************************************************/
    /**
     * \class   AbsoluteTests
     *
     * \brief   Class AbsoluteTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class AbsoluteTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public AbsoluteTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="AbsoluteTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public AbsoluteTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteHigherBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteHigherBoundary()
        {
            Assert.Equal(0xD0, Fixture.mem.ReadMem(0xFFFF, CPUAddressingMode.Absolute, 0, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteLowerBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteLowerBoundary()
        {
            Assert.Equal(0xC1, Fixture.mem.ReadMem(0, CPUAddressingMode.Absolute, 0, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteNormalBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteNormalBoundary()
        {
            Assert.Equal(0x5B,
                Fixture.mem.ReadMem(0x357E, CPUAddressingMode.Absolute, 0, 0).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   AbsoluteXTests
     *
     * \brief   Class AbsoluteXTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class AbsoluteXTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public AbsoluteXTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="AbsoluteXTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public AbsoluteXTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteXHigherBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteXHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteXHigherBoundary()
        {
            Assert.Equal(0x43, Fixture.mem.ReadMem(0xFFFF, CPUAddressingMode.AbsoluteX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteXLowerBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteXLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteXLowerBoundary()
        {
            Assert.Equal(0x9F, Fixture.mem.ReadMem(0, CPUAddressingMode.AbsoluteX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteXNormalBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteXNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteXNormalBoundary()
        {
            Assert.Equal(0x32, Fixture.mem.ReadMem(0x357E, CPUAddressingMode.AbsoluteX, 0x56, 0).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   AbsoluteYTests
     *
     * \brief   Class AbsoluteYTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class AbsoluteYTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public AbsoluteYTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="AbsoluteYTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public AbsoluteYTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteYHigherBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteYHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteYHigherBoundary()
        {
            Assert.Equal(0x43, Fixture.mem.ReadMem(0xFFFF, CPUAddressingMode.AbsoluteY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteYLowerBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteYLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteYLowerBoundary()
        {
            Assert.Equal(0x9F, Fixture.mem.ReadMem(0, CPUAddressingMode.AbsoluteY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckAbsoluteYNormalBoundary()
         *
         * \brief   Defines the test method CheckAbsoluteYNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckAbsoluteYNormalBoundary()
        {
            Assert.Equal(0x32, Fixture.mem.ReadMem(0x357E, CPUAddressingMode.AbsoluteY, 0, 0x56).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   IndirectTests
     *
     * \brief   Class IndirectTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class IndirectTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public IndirectTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="IndirectTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public IndirectTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectHigherBoundary()
         *
         * \brief   Defines the test method CheckIndirectHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectHigherBoundary()
        {
            Assert.Equal(0xC1D0, Fixture.mem.ReadMem(0xFFFF, CPUAddressingMode.Indirect, true));
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectLowerBoundary()
         *
         * \brief   Defines the test method CheckIndirectLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectLowerBoundary()
        {
            Assert.Equal(0x12C1, Fixture.mem.ReadMem(0, CPUAddressingMode.Indirect, true));
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectNormalBoundary()
         *
         * \brief   Defines the test method CheckIndirectNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectNormalBoundary()
        {
            Assert.Equal(0xB95B, Fixture.mem.ReadMem(0x357E, CPUAddressingMode.Indirect, true));
        }
    }

    /**********************************************************************************************/
    /**
     * \class   IndirectXTests
     *
     * \brief   Class IndirectXTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class IndirectXTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public IndirectXTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="IndirectXTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public IndirectXTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectXHigherBoundary()
         *
         * \brief   Defines the test method CheckIndirectXHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectXHigherBoundary()
        {
            Assert.Equal(0x9A, Fixture.mem.ReadMem(0xFF, CPUAddressingMode.IndirectX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectXLowerBoundary()
         *
         * \brief   Defines the test method CheckIndirectXLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectXLowerBoundary()
        {
            Assert.Equal(0xCA, Fixture.mem.ReadMem(0, CPUAddressingMode.IndirectX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectXNormalBoundary()
         *
         * \brief   Defines the test method CheckIndirectXNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectXNormalBoundary()
        {
            Assert.Equal(0x1A, Fixture.mem.ReadMem(0x7E, CPUAddressingMode.IndirectX, 0x56, 0).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   IndirectYTests
     *
     * \brief   Class IndirectYTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class IndirectYTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public IndirectYTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="IndirectYTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public IndirectYTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectYHigherBoundary()
         *
         * \brief   Defines the test method CheckIndirectYHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectYHigherBoundary()
        {
            Assert.Equal(0x08, Fixture.mem.ReadMem(0xFF, CPUAddressingMode.IndirectY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectYLowerBoundary()
         *
         * \brief   Defines the test method CheckIndirectYLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectYLowerBoundary()
        {
            Assert.Equal(0xB9, Fixture.mem.ReadMem(0, CPUAddressingMode.IndirectY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckIndirectYNormalBoundary()
         *
         * \brief   Defines the test method CheckIndirectYNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckIndirectYNormalBoundary()
        {
            Assert.Equal(0x81, Fixture.mem.ReadMem(0x7E, CPUAddressingMode.IndirectY, 0, 0x56).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   RelativeTests
     *
     * \brief   Class RelativeTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class RelativeTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public RelativeTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="RelativeTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public RelativeTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckrelativeThrowsNotSupportedException()
         *
         * \brief   Defines the test method CheckrelativeThrowsNotSupportedException.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckrelativeThrowsNotSupportedException()
        {
            Assert.Throws<NotSupportedException>(() => Fixture.mem.ReadMem(35, CPUAddressingMode.Relative, 0, 0));
        }
    }

    /**********************************************************************************************/
    /**
     * \class   ZeroPageTests
     *
     * \brief   Class ZeroPageTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class ZeroPageTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public ZeroPageTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="ZeroPageTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public ZeroPageTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageHigherBoundary()
         *
         * \brief   Defines the test method CheckZeroPageHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageHigherBoundary()
        {
            Assert.Equal(0xC1, Fixture.mem.ReadMem(0xFF, CPUAddressingMode.ZeroPage, 0, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageLowerBoundary()
         *
         * \brief   Defines the test method CheckZeroPageLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageLowerBoundary()
        {
            Assert.Equal(0xC1, Fixture.mem.ReadMem(0, CPUAddressingMode.ZeroPage, 0, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageNormalBoundary()
         *
         * \brief   Defines the test method CheckZeroPageNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageNormalBoundary()
        {
            Assert.Equal(0x7E, Fixture.mem.ReadMem(0x7E, CPUAddressingMode.ZeroPage, 0, 0).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   ZeroPageXTests
     *
     * \brief   Class ZeroPageXTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class ZeroPageXTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public ZeroPageXTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="ZeroPageXTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public ZeroPageXTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageXHigherBoundary()
         *
         * \brief   Defines the test method CheckZeroPageXHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageXHigherBoundary()
        {
            Assert.Equal(0x43, Fixture.mem.ReadMem(0xFF, CPUAddressingMode.ZeroPageX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageXLowerBoundary()
         *
         * \brief   Defines the test method CheckZeroPageXLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageXLowerBoundary()
        {
            Assert.Equal(0x9F, Fixture.mem.ReadMem(0, CPUAddressingMode.ZeroPageX, 0x56, 0).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageXNormalBoundary()
         *
         * \brief   Defines the test method CheckZeroPageXNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageXNormalBoundary()
        {
            Assert.Equal(0xC7, Fixture.mem.ReadMem(0x7E, CPUAddressingMode.ZeroPageX, 0x56, 0).data);
        }
    }

    /**********************************************************************************************/
    /**
     * \class   ZeroPageYTests
     *
     * \brief   Class ZeroPageYTests.
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    [Collection("MemoryCollection")]
    public class ZeroPageYTests
    {
        /**********************************************************************************************/
        /**
         * \fn  public ZeroPageYTests(MemoryTestFixture fixture)
         *
         * \brief   Initializes a new instance of the <see cref="ZeroPageYTests"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   fixture The fixture.
         **************************************************************************************************/
        public ZeroPageYTests(MemoryTestFixture fixture)
        {
            Fixture = fixture;
        }

        /** \brief   The fixture */
        private readonly MemoryTestFixture Fixture;

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageYHigherBoundary()
         *
         * \brief   Defines the test method CheckZeroPageYHigherBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageYHigherBoundary()
        {
            Assert.Equal(0x43, Fixture.mem.ReadMem(0xFF, CPUAddressingMode.ZeroPageY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageYLowerBoundary()
         *
         * \brief   Defines the test method CheckZeroPageYLowerBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageYLowerBoundary()
        {
            Assert.Equal(0x9F, Fixture.mem.ReadMem(0, CPUAddressingMode.ZeroPageY, 0, 0x56).data);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CheckZeroPageYNormalBoundary()
         *
         * \brief   Defines the test method CheckZeroPageYNormalBoundary.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [Fact]
        public void CheckZeroPageYNormalBoundary()
        {
            Assert.Equal(0xC7, Fixture.mem.ReadMem(0x7E, CPUAddressingMode.ZeroPageY, 0, 0x56).data);
        }
    }
}