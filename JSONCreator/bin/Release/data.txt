ADC
Add memory to accumulator with carry
2
ADC #Oper
69
2
2
3
ADC Oper
65
2
3
4
ADC Oper,X
75
2
4
7
ADC Oper
6D
3
4
8
ADC Oper,X
7D
3
4
9
ADC Oper,Y
79
3
4
11
ADC (Oper,X)
61
2
6
12
ADC (Oper),Y
71
2
5

AND
"AND" memory with accumulator
2
AND #Oper
29
2
2
3
AND Oper
25
2
3
4
AND Oper,X
35
2
4
7
AND Oper
2D
3
4
8
AND Oper,X
3D
3
4
9
AND Oper,Y
39
3
4
11
AND (Oper,X)
21
2
6
12
AND (Oper),Y
31
2
5

ASL
Shift Left One Bit (Memory or Accumulator)
1
ASL A
0A
1
2
3
ASL Oper
06
2
5
4
ASL Oper,X
16
2
6
7
ASL Oper
0E
3
6
8
ASL Oper,X
1E
3
7

BCC
Branch on Carry Clear
6
BCC Oper
90
2
2

BCS
Branch on carry set
6
BCS Oper
B0
2
2

BEQ
Branch on result zero
6
BEQ Oper
F0
2
2

BIT
Test bits in memory with accumulator
3
BIT Oper
24
2
3
7
BIT Oper
2C
3
4

BMI
Branch on result minus
6
BMI Oper
30
2
2

BNE
Branch on result not zero
6
BNE Oper
D0
2
2

BPL
Branch on result plus
6
BPL Oper
10
2
2

BRK
Force Break
13
BRK
00
1
7

BVC
Branch on overflow clear
6
BVC Oper
50
2
2

BVS
Branch on overflow set
6
BVS Oper
70
2
2

CLC
Clear carry flag
13
CLC
18
1
2

CLD
Clear decimal mode
13
CLD
D8
1
2

CLI
Clear interrupt disable bit
13
CLI
58
1
2

CLV
Clear overflow flag
13
CLV
B8
1
2

CMP
Compare memory and accumulator
2
CMP #Oper
C9
2
2
3
CMP Oper
C5
2
3
4
CMP Oper,X
D5
2
4
7
CMP Oper
CD
3
4
8
CMP Oper,X
DD
3
4
9
CMP Oper,Y
D9
3
4
11
CMP (Oper,X)
C1
2
6
12
CMP (Oper),Y
D1
2
5

CPX
Compare Memory and Index X
2
CPX Oper
E0
2
2
3
CPX Oper
E4
2
3
7
CPX Oper
EC
3
4

CPY
Compare memory and index Y
2
CPY Oper
C0
2
2
3
CPY Oper
C4
2
3
7
CPY Oper
CC
3
4

DEC
Decrement memory by one
3
DEC Oper
C6
2
5
4
DEC Oper,X
D6
2
6
7
DEC Oper
CE
3
6
8
DEC Oper,X
DE
3
7

DEX
Decrement index X by one
13
DEX
CA
1
2

DEY
Decrement index Y by one
13
DEY
88
1
2

EOR
"ExclusiveOr" memory with accumulator
2
EOR #Oper
49
2
2
3
EOR Oper
45
2
3
4
EOR Oper,X
55
2
4
7
EOR Oper
40
3
4
8
EOR Oper,X
5D
3
4
9
EOR Oper,Y
59
3
4
11
EOR (Oper,X)
41
2
6
12
EOR (Oper),Y
51
2
5

INC
Increment memory by one
3
INC Oper
E6
2
5
4
INC Oper,X
F6
2
6
7
INC Oper
EE
3
6
8
INC Oper,X
FE
3
7

INX
Increment Index X by one
13
INX
E8
1
2

INY
Increment Index Y by one
13
INY
C8
1
2

JMP
Jump to new location
7
JMP Oper
4C
3
3
10
JMP (Oper)
6C
3
5

JSR
Jump to new location saving return address
7
JSR Oper
20
3
6

LDA
Load accumulator with memory
2
LDA #Oper
A9
2
2
3
LDA Oper
A5
2
3
4
LDA Oper,X
B5
2
4
7
LDA Oper
AD
3
4
8
LDA Oper,X
BD
3
4
9
LDA Oper,Y
B9
3
4
11
LDA (Oper,X)
A1
2
6
12
LDA (Oper),Y
B1
2
5

LDX
Load index X with memory
2
LDX #Oper
A2
2
2
3
LDX Oper
A6
2
3
5
LDX Oper,Y
B6
2
4
7
LDX Oper
AE
3
4
9
LDX Oper,Y
BE
3
4

LDY
Load index Y with memory
2
LDY #Oper
A0
2
2
3
LDY Oper
A4
2
3
4
LDY Oper,X
B4
2
4
7
LDY Oper
AC
3
4
8
LDY Oper,X
BC
3
4

LSR
Shift right one bit (memory or accumulator)
1
LSR A
4A
1
2
3
LSR Oper
46
2
5
4
LSR Oper,X
56
2
6
7
LSR Oper
4E
3
6
8
LSR Oper,X
5E
3
7

NOP
No operation
13
NOP
EA
1
2

ORA
"OR" memory with accumulator
2
ORA #Oper
09
2
2
3
ORA Oper
05
2
3
4
ORA Oper,X
15
2
4
7
ORA Oper
0D
3
4
8
ORA Oper,X
1D
3
4
9
ORA Oper,Y
19
3
4
11
ORA (Oper,X)
01
2
6
12
ORA (Oper),Y
11
2
5

PHA
Push accumulator on stack
13
PHA
48
1
3

PHP
Push processor status on stack
13
PHP
08
1
3

PLA
Pull accumulator from stack
13
PLA
68
1
4

PLP
Pull processor status from stack
13
PLP
28
1
4

ROL
Rotate one bit left (memory or accumulator)
1
ROL A
2A
1
2
3
ROL Oper
26
2
5
4
ROL Oper,X
36
2
6
7
ROL Oper
2E
3
6
8
ROL Oper,X
3E
3
7

ROR
Rotate one bit right (memory or accumulator)
1
ROR A
6A
1
2
3
ROR Oper
66
2
5
4
ROR Oper,X
76
2
6
7
ROR Oper
6E
3
6
8
ROR Oper,X
7E
3
7

RTI
Return from interrupt
13
RTI
4D
1
6

RTS
Return from subroutine
13
RTS
60
1
6

SBC
Subtract memory from accumulator with borrow
2
SBC #Oper
E9
2
2
3
SBC Oper
E5
2
3
4
SBC Oper,X
F5
2
4
7
SBC Oper
ED
3
4
8
SBC Oper,X
FD
3
4
9
SBC Oper,Y
F9
3
4
11
SBC (Oper,X)
E1
2
6
12
SBC (Oper),Y
F1
2
5

SEC
Set carry flag
13
SEC
38
1
2

SED
Set decimal mode
13
SED
F8
1
2

SEI
Set interrupt disable status
13
SEI
78
1
2

STA
Store accumulator in memory
3
STA Oper
85
2
3
4
STA Oper,X
95
2
4
7
STA Oper
8D
3
4
8
STA Oper,X
9D
3
5
9
STA Oper, Y
99
3
5
11
STA (Oper,X)
81
2
6
12
STA (Oper),Y
91
2
6

STX
Store index X in memory
3
STX Oper
86
2
3
5
STX Oper,Y
96
2
4
7
STX Oper
8E
3
4

STY
Store index Y in memory
3
STY Oper
84
2
3
4
STY Oper,X
94
2
4
7
STY Oper
8C
3
4

TAX
Transfer accumulator to index X
13
TAX
AA
1
2

TAY
Transfer accumulator to index Y
13
TAY
A8
1
2

TSX
Transfer stack pointer to index X
13
TSX
BA
1
2

TXA
Transfer index X to accumulator
13
TXA
8A
1
2

TXS
Transfer index X to stack pointer
13
TXS
9A
1
2

TYA
Transfer index Y to accumulator
13
TYA
98
1
2