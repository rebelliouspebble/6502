﻿/**********************************************************************************************//**
 * \file    Program.cs.
 *
 * \brief   Implements the program class
 **************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using SystemEmulator;
using Newtonsoft.Json;

namespace JSONCreator
{
    /**********************************************************************************************//**
     * \class   Program
     *
     * \brief   Class Program.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    internal class Program
    {
        /**********************************************************************************************//**
         * \fn  public static void Main(string[] args)
         *
         * \brief   Defines the entry point of the application.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   args    The arguments.
         **************************************************************************************************/

        public static void Main(string[] args)
        {
            Encoder encoder;
            Instruction instruction;
            Operation operation;
            encoder = new Encoder();
            var lines = File.ReadAllText("data.txt");
            var splitinst = new List<string[]>();
            var instructions = lines.Split(new[] {Environment.NewLine + Environment.NewLine},
                StringSplitOptions.RemoveEmptyEntries);
            foreach (var i in instructions)
                splitinst.Add(i.Split(new[] {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries));

            foreach (var inst in splitinst)
            {
                instruction = new Instruction();
                instruction.mnemonic = inst[0];
                instruction.description = inst[1];
                for (var i = 2; i < inst.Length; i = i + 5)
                {
                    operation = new Operation();
                    switch (int.Parse(inst[i]))
                    {
                        case 1:
                            operation.adressingmode = CPUAddressingMode.Accumulator;
                            break;
                        case 2:
                            operation.adressingmode = CPUAddressingMode.Immediate;
                            break;
                        case 3:
                            operation.adressingmode = CPUAddressingMode.ZeroPage;
                            break;
                        case 4:
                            operation.adressingmode = CPUAddressingMode.ZeroPageX;
                            break;
                        case 5:
                            operation.adressingmode = CPUAddressingMode.ZeroPageY;
                            break;
                        case 6:
                            operation.adressingmode = CPUAddressingMode.Relative;
                            break;
                        case 7:
                            operation.adressingmode = CPUAddressingMode.Absolute;
                            break;
                        case 8:
                            operation.adressingmode = CPUAddressingMode.AbsoluteX;
                            break;
                        case 9:
                            operation.adressingmode = CPUAddressingMode.AbsoluteY;
                            break;
                        case 10:
                            operation.adressingmode = CPUAddressingMode.Indirect;
                            break;
                        case 11:
                            operation.adressingmode = CPUAddressingMode.IndirectX;
                            break;
                        case 12:
                            operation.adressingmode = CPUAddressingMode.IndirectY;
                            break;
                        case 13:
                            operation.adressingmode = CPUAddressingMode.Implicit;
                            break;
                    }

                    operation.format = inst[i + 1];
                    var opcode = int.Parse(inst[i + 2], NumberStyles.HexNumber);
                    operation.bytes = int.Parse(inst[i + 3]);
                    operation.cycles = int.Parse(inst[i + 4]);
                    instruction.Operations.Add(Convert.ToByte(opcode), operation);
                }

                encoder.Instructions.Add(instruction);
            }

            File.WriteAllText("instructions.json", JsonConvert.SerializeObject(encoder));
        }
    }
}