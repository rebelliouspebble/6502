/**********************************************************************************************//**
 * \file    JsonClasses.cs.
 *
 * \brief   Implements the JSON classes class
 **************************************************************************************************/

using System.Collections.Generic;
using SystemEmulator;
using Newtonsoft.Json;

namespace JSONCreator
{
    /**********************************************************************************************//**
     * \class   Encoder
     *
     * \brief   Class Encoder.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Encoder
    {
        /**********************************************************************************************//**
         * \fn  public Encoder()
         *
         * \brief   Initializes a new instance of the <see cref="Encoder" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public Encoder()
        {
            Instructions = new List<Instruction>();
        }

        /**********************************************************************************************//**
         * \property    public List<Instruction> Instructions
         *
         * \brief   Gets or sets the instruction list
         *
         * \returns The instructions.
         **************************************************************************************************/

        [JsonProperty("instructions")]
        public List<Instruction> Instructions { get; set; }
    }

    /**********************************************************************************************//**
     * \class   Instruction
     *
     * \brief   Class Instruction.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Instruction
    {
        /**********************************************************************************************//**
         * \fn  public Instruction()
         *
         * \brief   Initializes a new instance of the <see cref="Instruction" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public Instruction()
        {
            Operations = new Dictionary<byte, Operation>();
        }

        /**********************************************************************************************//**
         * \property    public string name
         *
         * \brief   Gets or sets the name.
         *
         * \returns The name.
         **************************************************************************************************/

        [JsonProperty("name")]
        public string name { get; set; }

        /**********************************************************************************************//**
         * \property    public string mnemonic
         *
         * \brief   Gets or sets the mnemonic.
         *
         * \returns The mnemonic.
         **************************************************************************************************/

        [JsonProperty("mnemonic")]
        public string mnemonic { get; set; }

        /**********************************************************************************************//**
         * \property    public string description
         *
         * \brief   Gets or sets the description.
         *
         * \returns The description.
         **************************************************************************************************/

        [JsonProperty("description")]
        public string description { get; set; }

        /**********************************************************************************************//**
         * \property    public Dictionary<byte, Operation> Operations
         *
         * \brief   Gets or sets the operations.
         *
         * \returns The operations.
         **************************************************************************************************/

        [JsonProperty("operations")]
        public Dictionary<byte, Operation> Operations { get; set; }
    }

    /**********************************************************************************************//**
     * \class   Operation
     *
     * \brief   Class Operation.
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Operation
    {
        /**********************************************************************************************//**
         * \property    public string format
         *
         * \brief   Gets or sets the format.
         *
         * \returns The format.
         **************************************************************************************************/

        [JsonProperty("format")]
        public string format { get; set; }

        /**********************************************************************************************//**
         * \property    public CPUAddressingMode adressingmode
         *
         * \brief   Gets or sets the adressingmode.
         *
         * \returns The adressingmode.
         **************************************************************************************************/

        [JsonProperty("addressingMode")]
        public CPUAddressingMode adressingmode { get; set; }

        /**********************************************************************************************//**
         * \property    public int cycles
         *
         * \brief   Gets or sets the cycles.
         *
         * \returns The cycles.
         **************************************************************************************************/

        [JsonProperty("cycles")]
        public int cycles { get; set; }

        /**********************************************************************************************//**
         * \property    public int bytes
         *
         * \brief   Gets or sets the bytes.
         *
         * \returns The bytes.
         **************************************************************************************************/

        [JsonProperty("bytes")]
        public int bytes { get; set; }
    }
}