/**********************************************************************************************//**
 * \file    Exceptions.cs.
 *
 * \brief   Implements the exceptions class
 **************************************************************************************************/

using System;

namespace SystemEmulator
{
    /**********************************************************************************************//**
     * \class   StackFullException
     *
     * \brief   Class StackFullException. Implements the <see cref="System.Exception" />
     *
     * \author  Glenn
     * \date    02/02/2020
     *
     * \sa  System.Exception
     **************************************************************************************************/

    public class StackFullException : Exception
    {
        /**********************************************************************************************//**
         * \fn  public StackFullException()
         *
         * \brief   Initializes a new instance of the <see cref="StackFullException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public StackFullException()
        {
        }

        /**********************************************************************************************//**
         * \fn  public StackFullException(string message) : base(message)
         *
         * \brief   Initializes a new instance of the <see cref="StackFullException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   message The message that describes the error.
         **************************************************************************************************/

        public StackFullException(string message) : base(message)
        {
        }

        /**********************************************************************************************//**
         * \fn  public StackFullException(string message, Exception inner) : base(message, inner)
         *
         * \brief   Initializes a new instance of the <see cref="StackFullException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   message The message.
         * \param   inner   Inner exception
         **************************************************************************************************/

        public StackFullException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    /**********************************************************************************************//**
     * \class   StackEmptyException
     *
     * \brief   Class StackEmptyException. Implements the <see cref="System.Exception" />
     *
     * \author  Glenn
     * \date    02/02/2020
     *
     * \sa  System.Exception
     **************************************************************************************************/

    public class StackEmptyException : Exception
    {
        /**********************************************************************************************//**
         * \fn  public StackEmptyException()
         *
         * \brief   Initializes a new instance of the <see cref="StackEmptyException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public StackEmptyException()
        {
        }

        /**********************************************************************************************//**
         * \fn  public StackEmptyException(string message) : base(message)
         *
         * \brief   Initializes a new instance of the <see cref="StackEmptyException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   message The message that describes the error.
         **************************************************************************************************/

        public StackEmptyException(string message) : base(message)
        {
        }

        /**********************************************************************************************//**
         * \fn  public StackEmptyException(string message, Exception inner) : base(message, inner)
         *
         * \brief   Initializes a new instance of the <see cref="StackEmptyException" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   message The message.
         * \param   inner   Inner exception
         **************************************************************************************************/

        public StackEmptyException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}