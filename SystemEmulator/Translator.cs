

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Newtonsoft.Json;

namespace SystemEmulator
{
    /**********************************************************************************************/
    /**
     * \struct  BasicOperation
     *
     * \brief   A structure containing essential instruction information
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    public struct BasicOperation
    {
        /** \brief   The instruction mmeonic */
        public string mme;

        /** \brief   The addressing mode for the instruction */
        public CPUAddressingMode AddressingMode;

        /** \brief   The amount of bytes the instruction requires */
        public int bytes;

        /** \brief   The amount of cycles the processor takes to complete the instruction */
        public int cycles;
    }

    /**********************************************************************************************/
    /**
     * \class   Translator
     *
     * \brief   The byte to instruction translator
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    public class Translator

    {
        /** \brief   All data */
        public readonly Encoder AllData;

        /** \brief   A dictionary to provide Byte to Instruction conversion */
        private readonly Dictionary<byte, BasicOperation> bytetoinst = new Dictionary<byte, BasicOperation>();

        /** \brief   A dictionary to provide Instruction to Byte conversion */
        private readonly Dictionary<(InstructionEnum inst, CPUAddressingMode addressingMode), byte> insttobyte =
            new Dictionary<(InstructionEnum inst, CPUAddressingMode addressingMode), byte>();

        /** \brief   The mmetodesc */
        private readonly Dictionary<string, string> mmetodesc = new Dictionary<string, string>();

        /**********************************************************************************************/
        /**
         * \fn  public Translator()
         *
         * \brief   Initializes a new instance of the <see cref="Translator"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public Translator()
        {
            AllData = JsonConvert.DeserializeObject<Encoder>(File.ReadAllText("instructions.json"));
            foreach (var instruction in AllData.Instructions)
            {
                foreach (var entry in instruction.Operations)
                {
                    var operation = new BasicOperation();
                    operation.mme = instruction.mnemonic;
                    operation.AddressingMode = entry.Value.adressingmode;
                    operation.bytes = entry.Value.bytes;
                    operation.cycles = entry.Value.cycles;
                    bytetoinst.Add(Convert.ToByte(entry.Key), operation);
                    insttobyte.Add(
                        (Utility.GetEnumValue<InstructionEnum>(instruction.mnemonic), entry.Value.adressingmode),
                        Convert.ToByte(entry.Key));
                }

                mmetodesc.Add(instruction.mnemonic, instruction.description);
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public string ReturnDescription(string mme)
         *
         * \brief   Returns a description
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   mme The mme.
         *
         * \returns The description.
         **************************************************************************************************/
        public string ReturnDescription(string mme)
        {
            return mmetodesc[mme];
        }

        /**********************************************************************************************/
        /**
         * \fn  public BasicOperation DecodeInstruction(byte opcode)
         *
         * \brief   Decodes the instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   opcode  The opcode.
         *
         * \returns BasicOperation.
         **************************************************************************************************/
        public BasicOperation DecodeInstruction(byte opcode)
        {
            return bytetoinst[opcode];
        }

        /**********************************************************************************************/
        /**
         * \fn  public byte EncodeInstruction(InstructionEnum inst, CPUAddressingMode addressingMode)
         *
         * \brief   Encodes the instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   inst            The inst.
         * \param   addressingMode  The addressing mode.
         *
         * \returns System.Byte.
         **************************************************************************************************/
        public byte EncodeInstruction(InstructionEnum inst, CPUAddressingMode addressingMode)
        {
            return insttobyte[(inst, addressingMode)];
        }
    }
}