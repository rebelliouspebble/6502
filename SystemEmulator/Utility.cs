﻿/**********************************************************************************************//**
 * \file    Utility.cs.
 *
 * \brief   Implements the utility class
 **************************************************************************************************/

using System;

namespace SystemEmulator
{
    /**********************************************************************************************//**
     * \class   Utility
     *
     * \brief   Utility functions
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Utility
    {
        /**********************************************************************************************//**
         * \fn  public static T GetEnumValue<T>(string str) where T : struct, IConvertible
         *
         * \brief   Gets the value of an enumerator when provided a string.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \exception   Exception   T must be an Enumeration type.
         *
         * \tparam  T   .
         * \param   str The string.
         *
         * \returns T.
         **************************************************************************************************/

        public static T GetEnumValue<T>(string str) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum) throw new Exception("T must be an Enumeration type.");
            var val = ((T[]) Enum.GetValues(typeof(T)))[0];
            if (!string.IsNullOrEmpty(str))
                foreach (var enumValue in (T[]) Enum.GetValues(typeof(T)))
                    if (enumValue.ToString().ToUpper().Equals(str.ToUpper()))
                    {
                        val = enumValue;
                        break;
                    }

            return val;
        }
    }
}