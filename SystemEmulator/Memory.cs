﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SystemEmulator
{
    /**********************************************************************************************/
    /**
     * \class   Memory
     *
     * \brief   An object representing processor and system memory
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    public class Memory
    {
        //TODO fix array sizing
        /** \brief   The physical byte array holding memory data */
        private byte[] memory = new byte[0x20000];

        /**********************************************************************************************/
        /**
         * \fn  public void overwriteMemory(byte[] newdata)
         *
         * \brief   Overwrites the memory.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   newdata The newdata.
         **************************************************************************************************/
        public void overwriteMemory(byte[] newdata)
        {
            memory = newdata;
        }

        /**********************************************************************************************/
        /**
         * \fn  public int ReadMem(int location, CPUAddressingMode addressingMode, bool ReturnIsAddressNotValue, byte registerValue = 0x00)
         *
         * \brief   Reads the memory.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   location                The location.
         * \param   addressingMode          The addressing mode.
         * \param   ReturnIsAddressNotValue if set to <c>true</c> [return is address not value].
         * \param   registerValue           (Optional) The register value.
         *
         * \returns System.Int32.
         **************************************************************************************************/
        public int ReadMem(int location, CPUAddressingMode addressingMode,
            bool ReturnIsAddressNotValue, byte registerValue = 0x00)
        {
            switch (addressingMode)
            {
                case CPUAddressingMode.Indirect:
                {
                    /*
                     The instruction contains a 16 bit address which identifies the location of the least significant byte of another 16 bit memory address which is the real target of the instruction.
                    For example if location $0120 contains $FC and location $0121 contains $BA then the instruction JMP ($0120) will cause the next instruction execution to occur at $BAFC (e.g. the contents of $0120 and $0121).
                    */
                    var targetAddress = new memoryLocation();
                    targetAddress.location = location;
                    var newLoc = new memoryLocation();
                    newLoc.byteLow = DirectReadMemory(targetAddress);
                    Debug.Print(targetAddress.location.ToString());
                    targetAddress.location += 1;
                    newLoc.byteHigh = DirectReadMemory(targetAddress.location & 0xFFFF);
                    Debug.Print("Indirect - Low Byte - " + newLoc.byteLow + Environment.NewLine);
                    Debug.Print("Indirect - High Byte - " + newLoc.byteHigh + Environment.NewLine);
                    return newLoc.location;
                }
            }

            return 0;
        }

        /**********************************************************************************************/
        /**
         * \fn  public (int location, bool pageCross) GetAddress(int location, CPUAddressingMode addressingMode, byte IndexRegisterX, byte IndexRegisterY)
         *
         * \brief   Returns a memory address
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \exception   ArgumentOutOfRangeException location.
         * \exception   NotSupportedException       Implement directly in processor, no RAM access used.
         *                                          or Implement directly in processor, values are
         *                                          literal. or Addressing mode must be implemented in
         *                                          the instruction or Addressing mode must be
         *                                          implemented in the instruction.
         * \exception   Exception                   This should not happen, ever. If this happens, might
         *                                          as well destroy the universe.
         *
         * \param   location        The location.
         * \param   addressingMode  The addressing mode.
         * \param   IndexRegisterX  The index register x.
         * \param   IndexRegisterY  The index register y.
         *
         * \returns System.ValueTuple&lt;System.Int32, System.Boolean&gt;.
         **************************************************************************************************/
        public (int location, bool pageCross) GetAddress(int location, CPUAddressingMode addressingMode,
            byte IndexRegisterX, byte IndexRegisterY)
        {
            if (location < 0) throw new ArgumentOutOfRangeException(nameof(location));
            switch (addressingMode)
            {
                case CPUAddressingMode.Absolute:
                {
                    return (location, false);
                }

                case CPUAddressingMode.AbsoluteX:
                {
                    return ((location + IndexRegisterX) & 0xFFFF, false);
                }

                case CPUAddressingMode.AbsoluteY:
                {
                    return ((location + IndexRegisterY) & 0xFFFF, false);
                }

                case CPUAddressingMode.Accumulator:
                {
                    throw new NotSupportedException("Implement directly in processor, no RAM access used.");
                }

                case CPUAddressingMode.Immediate:
                {
                    throw new NotSupportedException("Implement directly in processor, values are literal.");
                }

                case CPUAddressingMode.Implicit:
                {
                    throw new NotSupportedException("Addressing mode must be implemented in the instruction");
                }

                case CPUAddressingMode.ZeroPage:
                {
                    /*
                     * An instruction using zero page addressing mode has only an 8 bit address operand.
                     * This limits it to addressing only the first 256 bytes of memory (e.g. $0000 to $00FF) where the most significant byte of the address is always zero.
                     * In zero page mode only the least significant byte of the address is held in the instruction making it shorter by one byte (important for space saving) and one less memory fetch during execution (important for speed).
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    splitLocation.byteHigh = 0x00;
                    return (splitLocation.location, false);
                }

                case CPUAddressingMode.ZeroPageX:
                {
                    /*
                     * The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the X register to it.
                     * For example if the X register contains $0F and the instruction LDA $80,X is executed then the accumulator will be loaded from $008F (e.g. $80 + $0F => $8F).
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    if (splitLocation.byteLow + IndexRegisterX > 0xFF)
                    {
                        pageWrapped = true;
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterX) & 0xFF);
                    }
                    else
                    {
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterX) & 0xFF);
                    }

                    splitLocation.byteHigh = 0x00;
                    return (splitLocation.location, pageWrapped);
                }

                case CPUAddressingMode.ZeroPageY:
                {
                    /*
                     * The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the Y register to it.
                     * This mode can only be used with the LDX and STX instructions.
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    if (splitLocation.byteLow + IndexRegisterY > 0xFF)
                    {
                        pageWrapped = true;
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterY) & 0xFF);
                    }
                    else
                    {
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterY) & 0xFF);
                    }

                    splitLocation.byteHigh = 0x00;
                    return (splitLocation.location, pageWrapped);
                }

                case CPUAddressingMode.Relative:
                {
                    throw new NotSupportedException("Addressing mode must be implemented in the instruction");
                }

                case CPUAddressingMode.IndirectX:
                {
                    /*
                     * Indexed indirect (IndirectX) addressing is normally used in conjunction with a table of address held on zero page.
                     * The address of the table is taken from the instruction and the X register added to it (with zero page wrap around) to give the location of the least significant byte of the target address.
                     */
                    var initial = new memoryLocation();
                    initial.location = location;
                    // get target address
                    var targetAddress = (initial.byteLow + IndexRegisterX) & 0xFF;
                    Debug.Print(targetAddress.ToString());
                    var newLoc = new memoryLocation();
                    newLoc.byteLow = DirectReadMemory(targetAddress);
                    newLoc.byteHigh = DirectReadMemory(targetAddress + 1);
                    Debug.Print("IndirectX - Low Byte - " + newLoc.byteLow + Environment.NewLine);
                    Debug.Print("IndirectX - High Byte - " + newLoc.byteHigh + Environment.NewLine);
                    return (newLoc.location, false);
                }

                case CPUAddressingMode.IndirectY:
                {
                    /*
                     * Only used with JMP
                     * Indirect indirect (IndirectY) addressing is the most common indirection mode used on the 6502.
                     * In instruction contains the zero page location of the least significant byte of 16 bit address.
                     * The Y register is dynamically added to this value to generated the actual target address for operation.
                     */
                    var initial = new memoryLocation();
                    initial.location = location;
                    // get target address
                    var targetAddress = initial.byteLow;
                    var newLoc = new memoryLocation();
                    newLoc.byteLow = DirectReadMemory(targetAddress);
                    newLoc.byteHigh = DirectReadMemory((targetAddress + 1) & 0xFF);
                    Debug.Print("IndirectY - Low Byte - " + newLoc.byteLow + Environment.NewLine);
                    Debug.Print("IndirectY - High Byte - " + newLoc.byteHigh + Environment.NewLine);
                    newLoc.location = (newLoc.location + IndexRegisterY) & 0xFFFF;
                    Debug.Print(newLoc.location + Environment.NewLine);

                    return (newLoc.location, false);
                }
            }

            throw new Exception("This should not happen, ever. If this happens, might as well destroy the universe.");
        }

        /**********************************************************************************************/
        /**
         * \fn  public (byte data, bool pageCross) ReadMem(int location, CPUAddressingMode addressingMode, byte IndexRegisterX, byte IndexRegisterY)
         *
         * \brief   Reads the memory.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \exception   ArgumentOutOfRangeException location.
         * \exception   NotSupportedException       Implement directly in processor, no RAM access used.
         *                                          or Implement directly in processor, values are
         *                                          literal. or Addressing mode must be implemented in
         *                                          the instruction or Addressing mode must be
         *                                          implemented in the instruction.
         * \exception   Exception                   This should not happen, ever. If this happens, might
         *                                          as well destroy the universe.
         *
         * \param   location        The location.
         * \param   addressingMode  The addressing mode.
         * \param   IndexRegisterX  The index register x.
         * \param   IndexRegisterY  The index register y.
         *
         * \returns System.ValueTuple&lt;System.Byte, System.Boolean&gt;.
         **************************************************************************************************/
        public (byte data, bool pageCross) ReadMem(int location, CPUAddressingMode addressingMode,
            byte IndexRegisterX, byte IndexRegisterY)
        {
            if (location < 0) throw new ArgumentOutOfRangeException(nameof(location));
            switch (addressingMode)
            {
                case CPUAddressingMode.Absolute:
                {
                    return (DirectReadMemory(location), false);
                }

                case CPUAddressingMode.AbsoluteX:
                {
                    return (DirectReadMemory((location + IndexRegisterX) & 0xFFFF), false);
                }

                case CPUAddressingMode.AbsoluteY:
                {
                    return (DirectReadMemory((location + IndexRegisterY) & 0xFFFF), false);
                }

                case CPUAddressingMode.Accumulator:
                {
                    throw new NotSupportedException("Implement directly in processor, no RAM access used.");
                }

                case CPUAddressingMode.Immediate:
                {
                    throw new NotSupportedException("Implement directly in processor, values are literal.");
                }

                case CPUAddressingMode.Implicit:
                {
                    throw new NotSupportedException("Addressing mode must be implemented in the instruction");
                }

                case CPUAddressingMode.ZeroPage:
                {
                    /*
                     * An instruction using zero page addressing mode has only an 8 bit address operand.
                     * This limits it to addressing only the first 256 bytes of memory (e.g. $0000 to $00FF) where the most significant byte of the address is always zero.
                     * In zero page mode only the least significant byte of the address is held in the instruction making it shorter by one byte (important for space saving) and one less memory fetch during execution (important for speed).
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    splitLocation.byteHigh = 0x00;
                    return (DirectReadMemory(splitLocation), false);
                }

                case CPUAddressingMode.ZeroPageX:
                {
                    /*
                     * The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the X register to it.
                     * For example if the X register contains $0F and the instruction LDA $80,X is executed then the accumulator will be loaded from $008F (e.g. $80 + $0F => $8F).
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    if (splitLocation.byteLow + IndexRegisterX > 0xFF)
                    {
                        pageWrapped = true;
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterX) & 0xFF);
                    }
                    else
                    {
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterX) & 0xFF);
                    }

                    splitLocation.byteHigh = 0x00;
                    return (DirectReadMemory(splitLocation), pageWrapped);
                }

                case CPUAddressingMode.ZeroPageY:
                {
                    /*
                     * The address to be accessed by an instruction using indexed zero page addressing is calculated by taking the 8 bit zero page address from the instruction and adding the current value of the Y register to it.
                     * This mode can only be used with the LDX and STX instructions.
                     */
                    var pageWrapped = false;
                    var splitLocation = new memoryLocation();
                    splitLocation.location = location;
                    if (splitLocation.byteLow + IndexRegisterY > 0xFF)
                    {
                        pageWrapped = true;
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterY) & 0xFF);
                    }
                    else
                    {
                        splitLocation.byteLow = Convert.ToByte((splitLocation.byteLow + IndexRegisterY) & 0xFF);
                    }

                    splitLocation.byteHigh = 0x00;
                    return (DirectReadMemory(splitLocation), pageWrapped);
                }

                case CPUAddressingMode.Relative:
                {
                    throw new NotSupportedException("Addressing mode must be implemented in the instruction");
                }

                case CPUAddressingMode.IndirectX:
                {
                    /*
                     * Indexed indirect (IndirectX) addressing is normally used in conjunction with a table of address held on zero page.
                     * The address of the table is taken from the instruction and the X register added to it (with zero page wrap around) to give the location of the least significant byte of the target address.
                     */
                    var initial = new memoryLocation();
                    initial.location = location;
                    // get target address
                    var targetAddress = (initial.byteLow + IndexRegisterX) & 0xFF;
                    Debug.Print(targetAddress.ToString());
                    var newLoc = new memoryLocation();
                    newLoc.byteLow = DirectReadMemory(targetAddress);
                    newLoc.byteHigh = DirectReadMemory(targetAddress + 1);
                    Debug.Print("IndirectX - Low Byte - " + newLoc.byteLow + Environment.NewLine);
                    Debug.Print("IndirectX - High Byte - " + newLoc.byteHigh + Environment.NewLine);
                    return (DirectReadMemory(newLoc), false);
                }

                case CPUAddressingMode.IndirectY:
                {
                    /*
                     * Only used with JMP
                     * Indirect indirect (IndirectY) addressing is the most common indirection mode used on the 6502.
                     * In instruction contains the zero page location of the least significant byte of 16 bit address.
                     * The Y register is dynamically added to this value to generated the actual target address for operation.
                     */
                    var initial = new memoryLocation();
                    initial.location = location;
                    // get target address
                    var targetAddress = initial.byteLow;
                    var newLoc = new memoryLocation();
                    newLoc.byteLow = DirectReadMemory(targetAddress);
                    newLoc.byteHigh = DirectReadMemory((targetAddress + 1) & 0xFF);
                    Debug.Print("IndirectY - Low Byte - " + newLoc.byteLow + Environment.NewLine);
                    Debug.Print("IndirectY - High Byte - " + newLoc.byteHigh + Environment.NewLine);
                    newLoc.location = (newLoc.location + IndexRegisterY) & 0xFFFF;
                    Debug.Print(newLoc.location + Environment.NewLine);

                    return (DirectReadMemory(newLoc), false);
                }
            }

            throw new Exception("This should not happen, ever. If this happens, might as well destroy the universe.");
        }

        /**********************************************************************************************/
        /**
         * \fn  public byte DirectReadMemory(int location)
         *
         * \brief   Reads a byte from memory directly
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   location    The location.
         *
         * \returns System.Byte.
         **************************************************************************************************/
        public byte DirectReadMemory(int location)
        {
            return memory[location];
        }

        /**********************************************************************************************/
        /**
         * \fn  public byte DirectReadMemory(memoryLocation splitLocation)
         *
         * \brief   Reads a byte from memory directly
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   splitLocation   The split location.
         *
         * \returns System.Byte.
         **************************************************************************************************/
        public byte DirectReadMemory(memoryLocation splitLocation)
        {
            return memory[splitLocation.location];
        }

        /**********************************************************************************************/
        /**
         * \fn  public void WriteMemory(int location, byte value)
         *
         * \brief   Writes a single byte to a location in memory
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   location    The location.
         * \param   value       The value.
         **************************************************************************************************/
        public void WriteMemory(int location, byte value)
        {
            memory[location] = value;
        }

        /**********************************************************************************************/
        /**
         * \fn  public int GetLocation(byte lowByte, byte highByte)
         *
         * \brief   Returns a memory location when given the low and high byte.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   lowByte     The low byte.
         * \param   highByte    The high byte.
         *
         * \returns System.Int32.
         **************************************************************************************************/
        public int GetLocation(byte lowByte, byte highByte)
        {
            var buildLocation = new memoryLocation();
            buildLocation.byteLow = lowByte;
            buildLocation.byteHigh = highByte;
            return buildLocation.location;
        }

        /**********************************************************************************************/
        /**
         * \struct  memoryLocation
         *
         * \brief   Represents a location in memory made up of a low and high byte.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        [StructLayout(LayoutKind.Explicit)]
        public struct memoryLocation
        {
            /** \brief   The high byte */
            [FieldOffset(1)] public byte byteHigh;

            /** \brief   The low byte */
            [FieldOffset(0)] public byte byteLow;

            /** \brief   The memory location/offset */
            [FieldOffset(0)] public int location;
        }
    }
}