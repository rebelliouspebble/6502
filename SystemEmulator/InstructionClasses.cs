/**********************************************************************************************//**
 * \file    InstructionClasses.cs.
 *
 * \brief   Implements the instruction classes class
 **************************************************************************************************/

using System.Collections.Generic;
using Newtonsoft.Json;

namespace SystemEmulator
{
    /**********************************************************************************************//**
     * \class   Encoder
     *
     * \brief   Represents the json encoder/decoder
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Encoder
    {
        /**********************************************************************************************//**
         * \fn  public Encoder()
         *
         * \brief   Initializes a new instance of the <see cref="Encoder" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public Encoder()
        {
            Instructions = new List<Instruction>();
        }

        /**********************************************************************************************//**
         * \property    public List<Instruction> Instructions
         *
         * \brief   Gets or sets the instructions.
         *
         * \returns The instructions.
         **************************************************************************************************/

        [JsonProperty("instructions")]
        public List<Instruction> Instructions { get; set; }
    }

    /**********************************************************************************************//**
     * \class   Instruction
     *
     * \brief   Represents an instruction
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Instruction
    {
        /**********************************************************************************************//**
         * \fn  public Instruction()
         *
         * \brief   Initializes a new instance of the <see cref="Instruction" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         **************************************************************************************************/

        public Instruction()
        {
            Operations = new Dictionary<int, Operation>();
        }

        /**********************************************************************************************//**
         * \property    public string name
         *
         * \brief   Gets or sets the name.
         *
         * \returns The name.
         **************************************************************************************************/

        [JsonProperty("name")]
        public string name { get; set; }

        /**********************************************************************************************//**
         * \property    public string mnemonic
         *
         * \brief   Gets or sets the mnemonic.
         *
         * \returns The mnemonic.
         **************************************************************************************************/

        [JsonProperty("mnemonic")]
        public string mnemonic { get; set; }

        /**********************************************************************************************//**
         * \property    public string description
         *
         * \brief   Gets or sets the description.
         *
         * \returns The description.
         **************************************************************************************************/

        [JsonProperty("description")]
        public string description { get; set; }

        /**********************************************************************************************//**
         * \property    public Dictionary<int, Operation> Operations
         *
         * \brief   Gets or sets the operations.
         *
         * \returns The operations.
         **************************************************************************************************/

        [JsonProperty("operations")]
        public Dictionary<int, Operation> Operations { get; set; }
    }

    /**********************************************************************************************//**
     * \class   Operation
     *
     * \brief   Represents an operation
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Operation
    {
        /**********************************************************************************************//**
         * \property    public string format
         *
         * \brief   Gets or sets the format.
         *
         * \returns The format.
         **************************************************************************************************/

        [JsonProperty("format")]
        public string format { get; set; }

        /**********************************************************************************************//**
         * \property    public CPUAddressingMode adressingmode
         *
         * \brief   Gets or sets the adressingmode.
         *
         * \returns The adressingmode.
         **************************************************************************************************/

        [JsonProperty("addressingMode")]
        public CPUAddressingMode adressingmode { get; set; }

        /**********************************************************************************************//**
         * \property    public int cycles
         *
         * \brief   Gets or sets the amount of cycles.
         *
         * \returns The cycles.
         **************************************************************************************************/

        [JsonProperty("cycles")]
        public int cycles { get; set; }

        /**********************************************************************************************//**
         * \property    public int bytes
         *
         * \brief   Gets or sets the amount of bytes.
         *
         * \returns The bytes.
         **************************************************************************************************/

        [JsonProperty("bytes")]
        public int bytes { get; set; }
    }
}