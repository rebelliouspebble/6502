﻿namespace SystemEmulator
{
    /**********************************************************************************************/
    /**
     * \enum    CPUAddressingMode
     *
     * \brief   Enumerator for All Addressing Modes
     **************************************************************************************************/
    public enum CPUAddressingMode
    {
        /// <summary>
        ///     Accumulator Addressing Mode
        /// </summary>
        Accumulator = 1,

        /// <summary>
        ///     Immediate Addressing Mode
        /// </summary>
        Immediate = 2,

        /// <summary>
        ///     Zero Page Addressing Mode
        /// </summary>
        ZeroPage = 3,

        /// <summary>
        ///     Zero Page X Addressing Mode
        /// </summary>
        ZeroPageX = 4,

        /// <summary>
        ///     Zero Page Y Addressing Mode
        /// </summary>
        ZeroPageY = 5,

        /// <summary>
        ///     Relative Addressing Mode
        /// </summary>
        Relative = 6,

        /// <summary>
        ///     Absolute Addressing Mode
        /// </summary>
        Absolute = 7,

        /// <summary>
        ///     Absolute X Addressing Mode
        /// </summary>
        AbsoluteX = 8,

        /// <summary>
        ///     Absolute Y Addressing Mode
        /// </summary>
        AbsoluteY = 9,

        /// <summary>
        ///     Indirect Addressing Mode
        /// </summary>
        Indirect = 10,

        /// <summary>
        ///     Indirect X Addressing Mode
        /// </summary>
        IndirectX = 11,

        /// <summary>
        ///     Indirect Y Addressing Mode
        /// </summary>
        IndirectY = 12,

        /// <summary>
        ///     Implicit Addressing Mode
        /// </summary>
        Implicit = 13
    }
}