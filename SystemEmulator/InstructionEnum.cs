/**********************************************************************************************//**
 * \file    InstructionEnum.cs.
 *
 * \brief   Implements  instruction enum class
 **************************************************************************************************/

namespace SystemEmulator
{
    /**********************************************************************************************//**
     * \enum    InstructionEnum
     *
     * \brief   Enumerator for all different instructions
     **************************************************************************************************/

    public enum InstructionEnum
    {
        /// <summary>
        ///      lda
        /// </summary>
        LDA,

        /// <summary>
        ///      LDX
        /// </summary>
        LDX,

        /// <summary>
        ///      ldy
        /// </summary>
        LDY,

        /// <summary>
        ///      sta
        /// </summary>
        STA,

        /// <summary>
        ///      STX
        /// </summary>
        STX,

        /// <summary>
        ///      sty
        /// </summary>
        STY,

        /// <summary>
        ///      tax
        /// </summary>
        TAX,

        /// <summary>
        ///      tay
        /// </summary>
        TAY,

        /// <summary>
        ///      txa
        /// </summary>
        TXA,

        /// <summary>
        ///      tya
        /// </summary>
        TYA,

        /// <summary>
        ///      TSX
        /// </summary>
        TSX,

        /// <summary>
        ///      TXS
        /// </summary>
        TXS,

        /// <summary>
        ///      pha
        /// </summary>
        PHA,

        /// <summary>
        ///      PHP
        /// </summary>
        PHP,

        /// <summary>
        ///      pla
        /// </summary>
        PLA,

        /// <summary>
        ///      PLP
        /// </summary>
        PLP,

        /// <summary>
        ///      and
        /// </summary>
        AND,

        /// <summary>
        ///      eor
        /// </summary>
        EOR,

        /// <summary>
        ///      ora
        /// </summary>
        ORA,

        /// <summary>
        ///      bit
        /// </summary>
        BIT,

        /// <summary>
        ///      adc
        /// </summary>
        ADC,

        /// <summary>
        ///      SBC
        /// </summary>
        SBC,

        /// <summary>
        ///      CMP
        /// </summary>
        CMP,

        /// <summary>
        ///      CPX
        /// </summary>
        CPX,

        /// <summary>
        ///      cpy
        /// </summary>
        CPY,

        /// <summary>
        ///      inc
        /// </summary>
        INC,

        /// <summary>
        ///      inx
        /// </summary>
        INX,

        /// <summary>
        ///      iny
        /// </summary>
        INY,

        /// <summary>
        ///      decimal
        /// </summary>
        DEC,

        /// <summary>
        ///      dex
        /// </summary>
        DEX,

        /// <summary>
        ///      dey
        /// </summary>
        DEY,

        /// <summary>
        ///      asl
        /// </summary>
        ASL,

        /// <summary>
        ///      LSR
        /// </summary>
        LSR,

        /// <summary>
        ///      rol
        /// </summary>
        ROL,

        /// <summary>
        ///      ror
        /// </summary>
        ROR,

        /// <summary>
        ///      JMP
        /// </summary>
        JMP,

        /// <summary>
        ///      JSR
        /// </summary>
        JSR,

        /// <summary>
        ///      RTS
        /// </summary>
        RTS,

        /// <summary>
        ///      BCC
        /// </summary>
        BCC,

        /// <summary>
        ///      BCS
        /// </summary>
        BCS,

        /// <summary>
        ///      beq
        /// </summary>
        BEQ,

        /// <summary>
        ///      bmi
        /// </summary>
        BMI,

        /// <summary>
        ///      bne
        /// </summary>
        BNE,

        /// <summary>
        ///      BPL
        /// </summary>
        BPL,

        /// <summary>
        ///      BVC
        /// </summary>
        BVC,

        /// <summary>
        ///      BVS
        /// </summary>
        BVS,

        /// <summary>
        ///      CLC
        /// </summary>
        CLC,

        /// <summary>
        ///      CLD
        /// </summary>
        CLD,

        /// <summary>
        ///      cli
        /// </summary>
        CLI,

        /// <summary>
        ///      CLV
        /// </summary>
        CLV,

        /// <summary>
        ///      sec
        /// </summary>
        SEC,

        /// <summary>
        ///      sed
        /// </summary>
        SED,

        /// <summary>
        ///      sei
        /// </summary>
        SEI,

        /// <summary>
        ///      BRK
        /// </summary>
        BRK,

        /// <summary>
        ///      nop
        /// </summary>
        NOP,

        /// <summary>
        ///      rti
        /// </summary>
        RTI
    }
}