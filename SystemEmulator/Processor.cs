﻿using System;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Timers;

namespace SystemEmulator
{
    /**********************************************************************************************/
    /**
     * \class   Processor
     *
     * \brief   Represents the main system, and contains all processor functions and instructions
     *
     * \author  Glenn
     * \date    10/01/2020
     **************************************************************************************************/
    public class Processor
    {
        /** \brief   Represents the translator module */
        private readonly Translator Translator;

        /**********************************************************************************************/
        /**
         * \brief   An 8 bit register which is used for all arithmetic and logical operations (except
         *          increments and decrements)
         *          Contents can be stored and retrieved from memory or the stack
         **************************************************************************************************/
        public byte Accumulator;

        /**********************************************************************************************/
        /**
         * \brief   Break Command (B)
         *          Set when BRK is executed and an interrupt is generated to process it
         **************************************************************************************************/
        public bool B;

        /**********************************************************************************************/
        /**
         * \brief   Carry Flag (C)
         *          Set if the last operation caused an overflow from bit 7 or an underflow from bit 0
         *          Set during arithmetic, logic, and shift operations Can be set using SEC and CLC
         **************************************************************************************************/
        public bool C;

        //Define values
        /** \brief   The processor clock speed */
        public int clockSpeed;

        /** \brief   The amount of cycles the processor has completed */
        public int cyclecount;

        /**********************************************************************************************/
        /**
         * \brief   Decimal Mode (D)
         *          When the flag is set, the processor obeys the rules of BCD arithmetic Set using SED
         *          and cleared with CL
         **************************************************************************************************/
        public bool D;

        /**********************************************************************************************/
        /**
         * \brief   Interrupt Disable (I)
         *          Set if SEI instruction is executed If the flag is set, the processor will not respond
         *          to interrupts from devices Can only be cleared by CLI instruction
         **************************************************************************************************/
        public bool I;

        /**********************************************************************************************/
        /**
         * \brief   An 8 bit general purpose register Can be loaded and saved into memory, compared with
         *          values held in memory, or incremented and decremented Has one special function which
         *          is the ability to get a copy of the stack pointer or change its value
         **************************************************************************************************/
        public byte IndexRegisterX;

        /**********************************************************************************************/
        /**
         * \brief   An 8 bit general purpose register Can be loaded and saved into memory, compared with
         *          values held in memory, or incremented and decremented
         **************************************************************************************************/
        public byte IndexRegisterY;

        /** \brief   The interrupt flag */
        private bool interrupt;

        /** \brief   The irq trigger flag */
        private bool IRQtrigger;

        /** \brief   Shows if the last instruction was an interrupt */
        private bool lastinterrupt;

        /** \brief   Represents the system memory */
        public Memory Memory;

        /**********************************************************************************************/
        /**
         * \brief   Negative Flag (N)
         *          Set if the result of the last operation has bit 7 set to 1
         **************************************************************************************************/
        public bool N;

        /** \brief   The NMI trigger value */
        private bool NMItrigger;

        //Define Registers

        /**********************************************************************************************/
        /**
         * \brief   A 16 bit register that points to the next instruction to be executed. Value is
         *          modified as instructions are executed The value can be changed by a jump, branch,
         *          call, or by returning from a subroutine or interrupt
         **************************************************************************************************/
        public int ProgramCounter;

        /** \brief   Represents the stack in memory */
        public Stack stack;

        /** \brief   Signals for the CPU thread to stop */
        public bool threadstop = false;

        /**********************************************************************************************/
        /**
         * \brief   Overflow Flag (V)
         *          Set during arithmetic operations if the result has yielded an invalid 2' compliment
         *          result (64+64=-128)
         *          Determined by looking at the carry between bits 6 and 7, and bit 7 and the carry flag
         **************************************************************************************************/
        public bool V;

        /**********************************************************************************************/
        /**
         * \brief   Zero Flag (Z)
         *          Set if the result of the last operation was 0
         **************************************************************************************************/
        public bool Z;

        /**********************************************************************************************/
        /**
         * \fn  public Processor(int speed, Memory mem)
         *
         * \brief   Initializes a new instance of the <see cref="Processor"/> class.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   speed   The desired initial clock speed as the delay between operations
         * \param   mem     Memory object representing the system memory
         **************************************************************************************************/
        public Processor(int speed, Memory mem)
        {
            Memory = mem;
            stack = new Stack(Memory);
            Translator = new Translator();
            NMItrigger = false;
            IRQtrigger = false;
            I = true;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void doNMI()
         *
         * \brief   Process for what happens when an NMI event occurs
         *          Decrements Program Counter and calls a break to location FFFA
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void doNMI()
        {
            ProgramCounter--;
            BRK(false, 0xFFFA);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void doIRQ()
         *
         * \brief   Process for what happens when an IRQ event happens
         *          Decrements program counter and calls a break to location FFFE
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void doIRQ()
        {
            if (!I)
            {
                ProgramCounter--;
                BRK(false, 0xFFFE);
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void RequestInterrupt()
         *
         * \brief   Requests a processor interrupt
         *          Sets the IRQtrigger flag to True
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void RequestInterrupt()
        {
            IRQtrigger = true;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LoadProgram(string file, int offset, int entry)
         *
         * \brief   Loads a binary file (.bin) into memory and sets the entry point
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   file    The location of the binary file on the local computer.
         * \param   offset  The offset in memory to place the first byte of the file and every subsequent byte.
         * \param   entry   The memory location where the processor should begin execution.
         **************************************************************************************************/
        public void LoadProgram(string file, int offset, int entry)
        {
            var counter = 0;
            var filestream = File.Open(file, FileMode.Open);
            var binread = new BinaryReader(filestream);
            for (var i = offset; i <= offset + binread.BaseStream.Length - 1; i++)
                Memory.WriteMemory(i, binread.ReadByte());

            ProgramCounter = entry;
            binread.Close();
            filestream.Close();
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LoadProgram(int offset, byte[] file, int entry)
         *
         * \brief   Loads a byte array directly into memory
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   offset  The offset in memory to place the first byte of the file and every subsequent byte.
         * \param   file    The byte array containing the bytes of the program to be loaded
         * \param   entry   The memory location where the processor should begin execution.
         **************************************************************************************************/
        public void LoadProgram(int offset, byte[] file, int entry)
        {
            for (var i = 0; i <= file.Length - 1; i++)
                Memory.WriteMemory(i + offset, file[i]);
            ProgramCounter = entry;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void RunSingleInstruction(InstructionEnum inst, CPUAddressingMode addressingMode, short operand)
         *
         * \brief   Runs a single instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \exception   ArgumentOutOfRangeException Thrown if there is an invalid instruction
         *
         * \param   inst            The instruction to be performed
         * \param   addressingMode  The addressing mode the instruction should be performed with.
         * \param   operand         The operand for the instruction that is to be processed if required.
         **************************************************************************************************/
        public void RunSingleInstruction(InstructionEnum inst, CPUAddressingMode addressingMode, short operand)
        {
            switch (inst)
            {
                case InstructionEnum.LDA:
                    LDA(addressingMode, operand);
                    break;
                case InstructionEnum.LDX:
                    LDX(addressingMode, operand);
                    break;
                case InstructionEnum.LDY:
                    LDY(addressingMode, operand);
                    break;
                case InstructionEnum.STA:
                    LDY(addressingMode, operand);
                    break;
                case InstructionEnum.STX:
                    STX(addressingMode, operand);
                    break;
                case InstructionEnum.STY:
                    STY(addressingMode, operand);
                    break;
                case InstructionEnum.TAX:
                    TAX();
                    break;
                case InstructionEnum.TAY:
                    TAY();
                    break;
                case InstructionEnum.TXA:
                    TXA();
                    break;
                case InstructionEnum.TYA:
                    TYA();
                    break;
                case InstructionEnum.TSX:
                    TSX();
                    break;
                case InstructionEnum.TXS:
                    TXS();
                    break;
                case InstructionEnum.PHA:
                    PHA();
                    break;
                case InstructionEnum.PHP:
                    PHP();
                    break;
                case InstructionEnum.PLA:
                    PLA();
                    break;
                case InstructionEnum.PLP:
                    PLP();
                    break;
                case InstructionEnum.AND:
                    AND(addressingMode, operand);
                    break;
                case InstructionEnum.EOR:
                    EOR(addressingMode, operand);
                    break;
                case InstructionEnum.ORA:
                    ORA(addressingMode, operand);
                    break;
                case InstructionEnum.BIT:
                    BIT(addressingMode, operand);
                    break;
                case InstructionEnum.ADC:
                    ADC(addressingMode, operand);
                    break;
                case InstructionEnum.SBC:
                    SBC(addressingMode, operand);
                    break;
                case InstructionEnum.CMP:
                    CMP(addressingMode, operand);
                    break;
                case InstructionEnum.CPX:
                    CPX(addressingMode, operand);
                    break;
                case InstructionEnum.CPY:
                    CPY(addressingMode, operand);
                    break;
                case InstructionEnum.INC:
                    INC(addressingMode, operand);
                    break;
                case InstructionEnum.INX:
                    INX();
                    break;
                case InstructionEnum.INY:
                    INY();
                    break;
                case InstructionEnum.DEC:
                    DEC(addressingMode, operand);
                    break;
                case InstructionEnum.DEX:
                    DEX();
                    break;
                case InstructionEnum.DEY:
                    DEY();
                    break;
                case InstructionEnum.ASL:
                    ASL(addressingMode, operand);
                    break;
                case InstructionEnum.LSR:
                    LSR(addressingMode, operand);
                    break;
                case InstructionEnum.ROL:
                    ROL(addressingMode, operand);
                    break;
                case InstructionEnum.ROR:
                    ROR(addressingMode, operand);
                    break;
                case InstructionEnum.JMP:
                    JMP(addressingMode, operand);
                    break;
                case InstructionEnum.JSR:
                    JSR(addressingMode, operand);
                    break;
                case InstructionEnum.RTS:
                    RTS();
                    break;
                case InstructionEnum.BCC:
                    BCC(addressingMode, operand);
                    break;
                case InstructionEnum.BCS:
                    BCS(addressingMode, operand);
                    break;
                case InstructionEnum.BEQ:
                    BEQ(addressingMode, operand);
                    break;
                case InstructionEnum.BMI:
                    BMI(addressingMode, operand);
                    break;
                case InstructionEnum.BNE:
                    BNE(addressingMode, operand);
                    break;
                case InstructionEnum.BPL:
                    BPL(addressingMode, operand);
                    break;
                case InstructionEnum.BVC:
                    BVC(addressingMode, operand);
                    break;
                case InstructionEnum.BVS:
                    BVS(addressingMode, operand);
                    break;
                case InstructionEnum.CLC:
                    CLC();
                    break;
                case InstructionEnum.CLD:
                    CLD();
                    break;
                case InstructionEnum.CLI:
                    CLI();
                    break;
                case InstructionEnum.CLV:
                    CLV();
                    break;
                case InstructionEnum.SEC:
                    SEC();
                    break;
                case InstructionEnum.SED:
                    SED();
                    break;
                case InstructionEnum.SEI:
                    SEI();
                    break;
                case InstructionEnum.BRK:
                    BRK(true, 0xfffe);
                    break;
                case InstructionEnum.NOP:
                    NOP();
                    break;
                case InstructionEnum.RTI:
                    RTI();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void Reset()
         *
         * \brief   Resets the processor after loading a program so that execution can safely begin execution
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void Reset()
        {
            ProgramCounter = 0xFFFE;
            ProgramCounter = (Memory.DirectReadMemory(ProgramCounter) |
                              (Memory.DirectReadMemory(ProgramCounter + 1) << 8));
            I = false;
            lastinterrupt = false;
            IRQtrigger = false;
            NMItrigger = false;
        }


        /** \brief   Timer object that controls automatic execution*/
        public Timer cpuclock;

        /**********************************************************************************************/
        /**
         * \fn  public void setupTimer()
         *
         * \brief   Sets up the timer
         *          Must be run before the timer can be used
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void setupTimer()
        {
            cpuclock = new Timer();
            clockSpeed = 3;
            cpuclock.Interval = clockSpeed * 1500;
            cpuclock.Enabled = true;
            cpuclock.Stop();
            cpuclock.Elapsed += TimerTick;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TimerTick(object o , ElapsedEventArgs eventArgs)
         *
         * \brief   Controls what happens when the CpuClock lapses
         *          Causes the next instruction to be executed and the clock speed to be updated
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   o           An object to process.
         * \param   eventArgs   Elapsed event information.
         **************************************************************************************************/
        public void TimerTick(object o, ElapsedEventArgs eventArgs)
        {
            RunNextInstruction();
            cpuclock.Interval = clockSpeed;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TimerTick()
         *
         * \brief   TimerTick function without params for stepped execution
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TimerTick()
        {
            RunNextInstruction();
            cpuclock.Interval = clockSpeed;
        }


        /** \brief   The byte of the current operation */
        public byte currentopbyte = 0;

        /** \brief   BasicOperation object representing the current operation */
        public BasicOperation currentoperation;

        /** \brief   The Operand used for the current instruction */
        public short currentoperand;

        /**********************************************************************************************/
        /**
         * \fn  public void RunNextInstruction()
         *
         * \brief   Runs the next instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \exception   ArgumentOutOfRangeException .
         **************************************************************************************************/
        public void RunNextInstruction()
        {
            var instructionbyte = Memory.DirectReadMemory(ProgramCounter);
            currentoperation = Translator.DecodeInstruction(instructionbyte);
            Debug.Print("---Instruction---");
            Debug.Print("MME: " + currentoperation.mme);
            Debug.Print("CurrentPC: " + ProgramCounter);
            Debug.Print("Value at PC: " + Memory.DirectReadMemory(ProgramCounter));

            currentopbyte = Memory.DirectReadMemory(ProgramCounter);

            var inst = Utility.GetEnumValue<InstructionEnum>(currentoperation.mme);
            var instsplit = new Memory.memoryLocation();
            var data = new byte[2];
            for (var i = 1; i < currentoperation.bytes; i++)
                data[i - 1] = Memory.DirectReadMemory(ProgramCounter + i);

            short operand;
            if (currentoperation.bytes == 3)
            {
                instsplit.byteHigh = data[0];
                instsplit.byteLow = data[1];
                operand = (short) instsplit.location;
            }
            else
            {
                operand = data[0];
            }

            currentoperand = operand;

            switch (inst)
            {
                case InstructionEnum.LDA:
                    LDA(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.LDX:
                    LDX(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.LDY:
                    LDY(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.STA:
                    LDY(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.STX:
                    STX(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.STY:
                    STY(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.TAX:
                    TAX();
                    break;
                case InstructionEnum.TAY:
                    TAY();
                    break;
                case InstructionEnum.TXA:
                    TXA();
                    break;
                case InstructionEnum.TYA:
                    TYA();
                    break;
                case InstructionEnum.TSX:
                    TSX();
                    break;
                case InstructionEnum.TXS:
                    TXS();
                    break;
                case InstructionEnum.PHA:
                    PHA();
                    break;
                case InstructionEnum.PHP:
                    PHP();
                    break;
                case InstructionEnum.PLA:
                    PLA();
                    break;
                case InstructionEnum.PLP:
                    PLP();
                    break;
                case InstructionEnum.AND:
                    AND(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.EOR:
                    EOR(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.ORA:
                    ORA(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BIT:
                    BIT(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.ADC:
                    ADC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.SBC:
                    SBC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.CMP:
                    CMP(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.CPX:
                    CPX(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.CPY:
                    CPY(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.INC:
                    INC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.INX:
                    INX();
                    break;
                case InstructionEnum.INY:
                    INY();
                    break;
                case InstructionEnum.DEC:
                    DEC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.DEX:
                    DEX();
                    break;
                case InstructionEnum.DEY:
                    DEY();
                    break;
                case InstructionEnum.ASL:
                    ASL(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.LSR:
                    LSR(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.ROL:
                    ROL(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.ROR:
                    ROR(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.JMP:
                    JMP(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.JSR:
                    JSR(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.RTS:
                    RTS();
                    break;
                case InstructionEnum.BCC:
                    BCC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BCS:
                    BCS(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BEQ:
                    BEQ(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BMI:
                    BMI(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BNE:
                    BNE(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BPL:
                    BPL(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BVC:
                    BVC(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.BVS:
                    BVS(currentoperation.AddressingMode, operand);
                    break;
                case InstructionEnum.CLC:
                    CLC();
                    break;
                case InstructionEnum.CLD:
                    CLD();
                    break;
                case InstructionEnum.CLI:
                    CLI();
                    break;
                case InstructionEnum.CLV:
                    CLV();
                    break;
                case InstructionEnum.SEC:
                    SEC();
                    break;
                case InstructionEnum.SED:
                    SED();
                    break;
                case InstructionEnum.SEI:
                    SEI();
                    break;
                case InstructionEnum.BRK:
                    BRK(true, 0xFFFE);
                    break;
                case InstructionEnum.NOP:
                    NOP();
                    break;
                case InstructionEnum.RTI:
                    RTI();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            ProgramCounter += (short) currentoperation.bytes;
            AddCycle(currentoperation.cycles);
            if (lastinterrupt)
            {
                if (NMItrigger)
                {
                    doNMI();
                    NMItrigger = false;
                }
                else if (IRQtrigger)
                {
                    doIRQ();
                    IRQtrigger = false;
                }
            }

            lastinterrupt = interrupt;
            interrupt = NMItrigger || IRQtrigger && !I;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void AddCycle()
         *
         * \brief   Adds one cycle to the cycle count
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void AddCycle()
        {
            cyclecount++;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void AddCycle(int amount)
         *
         * \brief   Adds x amount of cycles to the cycle count
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   amount  The amount.
         **************************************************************************************************/
        public void AddCycle(int amount)
        {
            cyclecount += amount;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ResetCycleCount()
         *
         * \brief   Resets the cycle count.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void ResetCycleCount()
        {
            cyclecount = 0;
        }

        /**********************************************************************************************/
        /**
         * \fn  public int GetCycleCount()
         *
         * \brief   Gets the cycle count.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \returns System.Int32.
         **************************************************************************************************/
        public int GetCycleCount()
        {
            return cyclecount;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SetNegativeFlag(byte value)
         *
         * \brief   Sets the negative flag.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   value   The value.
         **************************************************************************************************/
        public void SetNegativeFlag(byte value)
        {
            N = value > 127;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SetZeroFlag(byte value)
         *
         * \brief   Sets the zero flag.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   value   The value.
         **************************************************************************************************/
        public void SetZeroFlag(byte value)
        {
            Z = value == 0;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void byteToStatus(byte statusbyte)
         *
         * \brief   Takes a status byte and imports its values onto the processor flags
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   statusbyte  The statusbyte.
         **************************************************************************************************/
        public void byteToStatus(byte statusbyte)
        {
            var BA = new BitArray(new[] {statusbyte});
            N = BA[0];
            V = BA[1];
            B = BA[3];
            D = BA[4];
            I = BA[5];
            Z = BA[6];
            C = BA[7];
        }

        /**********************************************************************************************/
        /**
         * \fn  public byte ConvertBitArrayToByte(BitArray bits)
         *
         * \brief   Converts a bit array to a single byte Requires an 8 bit BitArray
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \exception   ArgumentException   bits.
         *
         * \param   bits    The bits.
         *
         * \returns System.Byte.
         **************************************************************************************************/
        public byte ConvertBitArrayToByte(BitArray bits)
        {
            if (bits.Count != 8) throw new ArgumentException("bits");
            var reversed = new BitArray(bits.Cast<bool>().Reverse().ToArray());
            var bytes = new byte[1];
            bits.CopyTo(bytes, 0);
            return bytes[0];
        }

        /**********************************************************************************************/
        /**
         * \fn  public byte statusToByte()
         *
         * \brief   Converts the status flags to a single byte
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \returns System.Byte.
         **************************************************************************************************/
        public byte statusToByte()
        {
            var BA = new BitArray(8);
            N = BA[0];
            V = BA[1];
            B = BA[3];
            D = BA[4];
            I = BA[5];
            Z = BA[6];
            C = BA[7];
            return ConvertBitArrayToByte(BA);
        }

        /**********************************************************************************************/
        /**
         * \fn  public BitArray statusToBitArray()
         *
         * \brief   Status to bit array
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \returns A BitArray.
         **************************************************************************************************/
        public BitArray statusToBitArray()
        {
            var BA = new BitArray(8);
            N = BA[0];
            V = BA[1];
            B = BA[3];
            D = BA[4];
            I = BA[5];
            Z = BA[6];
            C = BA[7];
            return BA;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ADC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the ADC instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void ADC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = memoryvalue + Accumulator + (C ? 1 : 0);

            V = ((Accumulator ^ newvalue) & 0x80) != 0 && ((Accumulator ^ memoryvalue) & 0x80) == 0;

            if (D)
            {
                newvalue = int.Parse(memoryvalue.ToString("x")) + int.Parse(Accumulator.ToString("x")) + (C ? 1 : 0);
                if (newvalue > 99)
                {
                    C = true;
                    newvalue -= 100;
                }
                else
                {
                    C = false;
                }

                newvalue = (int) Convert.ToInt64(string.Concat("0x", newvalue), 16);
            }
            else
            {
                if (newvalue > 255)
                {
                    C = true;
                    newvalue -= 256;
                }
                else
                {
                    C = false;
                }
            }

            SetZeroFlag((byte) newvalue);
            SetNegativeFlag((byte) newvalue);

            Accumulator = Convert.ToByte(newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void AND(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the AND instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void AND(CPUAddressingMode addressingMode, short value)
        {
            int memoryvalue;
            bool page;
            (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate ||
                                  addressingMode == CPUAddressingMode.Accumulator ||
                                  addressingMode == CPUAddressingMode.Implicit
                ? (value, false)
                : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            if (page) AddCycle();

            Accumulator &= Convert.ToByte(value);

            SetZeroFlag(Accumulator);
            SetNegativeFlag(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ASL(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the ASL instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void ASL(CPUAddressingMode addressingMode, short value)
        {
            int newvalue;
            //get relevant values
            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                newvalue = Accumulator;
            }
            else
            {
                newvalue = value;
            }

            //shift data
            C = ((newvalue & 0x80) != 0);
            newvalue = (newvalue << 1) & 0xFE;

            SetNegativeFlag((byte) newvalue);
            SetZeroFlag((byte) newvalue);

            //store data back
            if (addressingMode == CPUAddressingMode.Accumulator)
                Accumulator = (byte) newvalue;
            else
            {
                Memory.WriteMemory(ProgramCounter + 1, (byte) newvalue);
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BCC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BCC instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BCC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (C == false)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BCS(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BCS instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BCS(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }


            if (C)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BEQ(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BEQ instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BEQ(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (Z)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BIT(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BIT instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BIT(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            var result = Convert.ToByte(Accumulator & memoryvalue);

            SetZeroFlag(result);

            var BA = new BitArray(new[] {result});

            V = BA[5];
            N = BA[6];
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BMI(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BMI instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BMI(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (N)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BNE(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BNE instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BNE(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (Z == false)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BPL(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BPL instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BPL(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (N == false)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BRK(bool isbrk, int lowLoc)
         *
         * \brief   Represents the BRK instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   isbrk   if set to <c>true</c> [isbrk].
         * \param   lowLoc  The low loc.
         **************************************************************************************************/
        public void BRK(bool isbrk, int lowLoc)
        {
            var splitPC = new Memory.memoryLocation();
            splitPC.location = ProgramCounter;
            stack.Push(splitPC.byteHigh);
            stack.Push(splitPC.byteLow);
            stack.Push(statusToByte());

            var IRQ = new Memory.memoryLocation();

            IRQ.byteLow = Memory.DirectReadMemory(lowLoc);
            IRQ.byteHigh = Memory.DirectReadMemory(lowLoc + 1);
            if (isbrk) B = true;
            I = true;
            lastinterrupt = false;
            ProgramCounter = (short) IRQ.location;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BVC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BVC instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BVC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (V == false)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void BVS(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the BVS instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void BVS(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (V)
            {
                ProgramCounter += (short) memoryvalue;
                if (page) AddCycle();
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CLC()
         *
         * \brief   Represents the CLC instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void CLC()
        {
            C = false;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CLD()
         *
         * \brief   Represents the CLD instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void CLD()
        {
            D = false;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CLI()
         *
         * \brief   Represents the CLI instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void CLI()
        {
            I = false;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CLV()
         *
         * \brief   Represents the CLV instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void CLV()
        {
            V = false;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CMP(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the CMP instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void CMP(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            if (Accumulator >= memoryvalue)
            {
                C = true;
                Z = false;
            }
            else
            {
                C = false;
                Z = true;
            }

            SetNegativeFlag(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CPX(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the CPX instruction
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void CPX(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            if (IndexRegisterX >= memoryvalue)
            {
                C = true;
                Z = false;
            }
            else
            {
                C = false;
                Z = true;
            }

            SetNegativeFlag(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void CPY(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the CPY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void CPY(CPUAddressingMode addressingMode, short value)
        {
            int memoryvalue;
            bool page;
            (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate ||
                                  addressingMode == CPUAddressingMode.Accumulator ||
                                  addressingMode == CPUAddressingMode.Implicit
                ? (value, false)
                : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            if (page) AddCycle();

            if (IndexRegisterY >= memoryvalue)
            {
                C = true;
                Z = false;
            }
            else
            {
                C = false;
                Z = true;
            }

            SetNegativeFlag(IndexRegisterY);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void DEC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the DEC instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void DEC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = memoryvalue - 1;
            SetZeroFlag((byte) newvalue);
            SetNegativeFlag((byte) newvalue);
            Memory.WriteMemory(value, (byte) newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void DEX()
         *
         * \brief   Represents the DEX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void DEX()
        {
            IndexRegisterX -= 1;
            SetZeroFlag(IndexRegisterX);
            SetNegativeFlag(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void DEY()
         *
         * \brief   Represents the DEY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void DEY()
        {
            IndexRegisterY -= 1;
            SetZeroFlag(IndexRegisterY);
            SetNegativeFlag(IndexRegisterY);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void EOR(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the EOR instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void EOR(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = Accumulator ^ memoryvalue;

            Accumulator = (byte) newvalue;
            SetNegativeFlag(Accumulator);
            SetZeroFlag(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void INC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the INC instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void INC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = memoryvalue + 1;
            SetZeroFlag((byte) newvalue);
            SetNegativeFlag((byte) newvalue);
            Memory.WriteMemory(value, (byte) newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void INX()
         *
         * \brief   Represents the INX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void INX()
        {
            IndexRegisterX += 1;
            SetZeroFlag(IndexRegisterX);
            SetNegativeFlag(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void INY()
         *
         * \brief   Represents the INY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void INY()
        {
            IndexRegisterY += 1;
            SetZeroFlag(IndexRegisterY);
            SetNegativeFlag(IndexRegisterY);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void JMP(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the JMP instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void JMP(CPUAddressingMode addressingMode, short value)
        {
            switch (addressingMode)
            {
                case CPUAddressingMode.Absolute:
                {
                    var newvalue = 0;
                    int memoryvalue;
                    bool page;

                    if (addressingMode == CPUAddressingMode.Accumulator)
                    {
                        memoryvalue = Accumulator;
                        page = false;
                    }

                    else
                    {
                        (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                            ? (value, false)
                            : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
                    }

                    ProgramCounter = (short) memoryvalue;
                    break;
                }

                case CPUAddressingMode.Indirect:
                {
                    var newvalue = 0;
                    int memoryvalue;
                    bool page;
                    memoryvalue = Memory.ReadMem(value, addressingMode, true);

                    ProgramCounter = (short) memoryvalue;
                    break;
                }
            }
        }

        /**********************************************************************************************/
        /**
         * \fn  public void JSR(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the JSR instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void JSR(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            var splitPC = new Memory.memoryLocation();
            splitPC.location = ProgramCounter + 1;
            stack.Push(splitPC.byteHigh);
            stack.Push(splitPC.byteLow);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LDA(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the LDA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void LDA(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            Accumulator = (byte) memoryvalue;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LDX(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the LDX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void LDX(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            IndexRegisterX = (byte) memoryvalue;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LDY(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the LDY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void LDY(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            Accumulator = (byte) memoryvalue;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void LSR(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the LSR instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void LSR(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            if (addressingMode == CPUAddressingMode.Accumulator) memoryvalue = Accumulator;

            N = false;
            C = (memoryvalue & 0x01) != 0;

            newvalue = (short) (newvalue >> 1);


            if (addressingMode == CPUAddressingMode.Accumulator)
                Accumulator = (byte) value;
            else
                Memory.WriteMemory(value, (byte) newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void NOP()
         *
         * \brief   Represents the NOP instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void NOP()
        {
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ORA(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the ORA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void ORA(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = Accumulator | memoryvalue;

            Accumulator = (byte) newvalue;
            SetNegativeFlag(Accumulator);
            SetZeroFlag(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void PHA()
         *
         * \brief   Represents the PHA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void PHA()
        {
            stack.Push(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void PHP()
         *
         * \brief   Represents the PHP instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void PHP()
        {
            stack.Push(statusToByte());
        }

        /**********************************************************************************************/
        /**
         * \fn  public void PLA()
         *
         * \brief   Represents the PLA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void PLA()
        {
            Accumulator = stack.Pop();
        }

        /**********************************************************************************************/
        /**
         * \fn  public void PLP()
         *
         * \brief   Represents the PLP instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void PLP()
        {
            byteToStatus(stack.Pop());
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ROL(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the ROL instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void ROL(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            if (addressingMode == CPUAddressingMode.Accumulator) memoryvalue = Accumulator;

            var carryflag = (0x80 & value) != 0;

            newvalue = (value << 1) & 0xFE;

            if (C) newvalue = newvalue | 0x01;

            C = carryflag;


            SetNegativeFlag((byte) newvalue);
            SetZeroFlag((byte) newvalue);
            if (addressingMode == CPUAddressingMode.Accumulator)
                Accumulator = (byte) newvalue;
            else
                Memory.WriteMemory(value, (byte) newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void ROR(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the ROR instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void ROR(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            if (addressingMode == CPUAddressingMode.Accumulator) memoryvalue = Accumulator;

            var carryflag = (0x01 & value) != 0;

            newvalue = (value >> 1) & 0xFE;

            if (C) newvalue = newvalue | 0x80;

            C = carryflag;


            SetNegativeFlag((byte) newvalue);
            SetZeroFlag((byte) newvalue);
            if (addressingMode == CPUAddressingMode.Accumulator)
                Accumulator = (byte) newvalue;
            else
                Memory.WriteMemory(value, (byte) newvalue);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void RTI()
         *
         * \brief   Represents the RTI instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void RTI()
        {
            byteToStatus(stack.Pop());
            var splitPC = new Memory.memoryLocation();
            splitPC.byteLow = stack.Pop();
            splitPC.byteHigh = stack.Pop();
            ProgramCounter = (short) splitPC.location;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void RTS()
         *
         * \brief   Represents the RTS instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void RTS()
        {
            var splitPC = new Memory.memoryLocation();
            splitPC.byteLow = stack.Pop();
            splitPC.byteHigh = stack.Pop();
            ProgramCounter = (short) (splitPC.location + 1);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SBC(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the SBC instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void SBC(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int memoryvalue;
            bool page;

            if (addressingMode == CPUAddressingMode.Accumulator)
            {
                memoryvalue = Accumulator;
                page = false;
            }

            else
            {
                (memoryvalue, page) = addressingMode == CPUAddressingMode.Immediate
                    ? (value, false)
                    : Memory.ReadMem(value, addressingMode, IndexRegisterX, IndexRegisterY);
            }

            if (page) AddCycle();

            newvalue = D
                ? int.Parse(Accumulator.ToString("x")) - int.Parse(memoryvalue.ToString("x")) - (C ? 0 : 1)
                : Accumulator - memoryvalue - (C ? 0 : 1);

            C = newvalue >= 0;

            if (D)
            {
                if (newvalue < 0)
                    newvalue += 100;

                newvalue = (int) Convert.ToInt64(string.Concat("0x", newvalue), 16);
            }
            else
            {
                V = ((Accumulator ^ newvalue) & 0x80) != 0 && ((Accumulator ^ memoryvalue) & 0x80) != 0;

                if (newvalue < 0)
                    newvalue += 256;
            }

            SetNegativeFlag((byte) newvalue);
            SetZeroFlag((byte) newvalue);

            Accumulator = (byte) newvalue;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SEC()
         *
         * \brief   Represents the SEC instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void SEC()
        {
            C = true;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SED()
         *
         * \brief   Represents the SED instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void SED()
        {
            D = true;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void SEI()
         *
         * \brief   Represents the SEI instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void SEI()
        {
            I = true;
        }

        /**********************************************************************************************/
        /**
         * \fn  public void STA(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the STA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void STA(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int location;
            bool page;
            (location, page) = addressingMode == CPUAddressingMode.Immediate ||
                               addressingMode == CPUAddressingMode.Accumulator ||
                               addressingMode == CPUAddressingMode.Implicit
                ? (value, false)
                : Memory.GetAddress(value, addressingMode, IndexRegisterX, IndexRegisterY);
            if (page) AddCycle();

            Memory.WriteMemory(location, Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void STX(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the STX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void STX(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int location;
            bool page;
            (location, page) = addressingMode == CPUAddressingMode.Immediate ||
                               addressingMode == CPUAddressingMode.Accumulator ||
                               addressingMode == CPUAddressingMode.Implicit
                ? (value, false)
                : Memory.GetAddress(value, addressingMode, IndexRegisterX, IndexRegisterY);
            if (page) AddCycle();

            Memory.WriteMemory(location, IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void STY(CPUAddressingMode addressingMode, short value)
         *
         * \brief   Represents the STY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         *
         * \param   addressingMode  The addressing mode.
         * \param   value           The value.
         **************************************************************************************************/
        public void STY(CPUAddressingMode addressingMode, short value)
        {
            var newvalue = 0;
            int location;
            bool page;
            (location, page) = addressingMode == CPUAddressingMode.Immediate ||
                               addressingMode == CPUAddressingMode.Accumulator ||
                               addressingMode == CPUAddressingMode.Implicit
                ? (value, false)
                : Memory.GetAddress(value, addressingMode, IndexRegisterX, IndexRegisterY);
            if (page) AddCycle();

            Memory.WriteMemory(location, IndexRegisterY);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TAX()
         *
         * \brief   Represents the TAX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TAX()
        {
            IndexRegisterX = Accumulator;
            SetZeroFlag(IndexRegisterX);
            SetNegativeFlag(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TAY()
         *
         * \brief   Represents the TAY instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TAY()
        {
            IndexRegisterY = Accumulator;
            SetZeroFlag(IndexRegisterY);
            SetNegativeFlag(IndexRegisterY);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TSX()
         *
         * \brief   Represents the TSX instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TSX()
        {
            IndexRegisterX = stack.GetPointer();
            SetZeroFlag(IndexRegisterX);
            SetNegativeFlag(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TXA()
         *
         * \brief   Represents the TXA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TXA()
        {
            Accumulator = IndexRegisterX;
            SetNegativeFlag(Accumulator);
            SetZeroFlag(Accumulator);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TXS()
         *
         * \brief   Represents the TXS instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TXS()
        {
            stack.SetPointer(IndexRegisterX);
        }

        /**********************************************************************************************/
        /**
         * \fn  public void TYA()
         *
         * \brief   Represents the TYA instruction.
         *
         * \author  Glenn
         * \date    10/01/2020
         **************************************************************************************************/
        public void TYA()
        {
            Accumulator = IndexRegisterY;
            SetNegativeFlag(Accumulator);
            SetZeroFlag(Accumulator);
        }
    }
}