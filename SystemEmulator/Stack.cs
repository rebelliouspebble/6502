/**********************************************************************************************//**
 * \file    Stack.cs.
 *
 * \brief   Implements the stack class
 **************************************************************************************************/

namespace SystemEmulator
{
    /**********************************************************************************************//**
     * \class   Stack
     *
     * \brief   Implements a descending stack in memory space
     *
     * \author  Glenn
     * \date    02/02/2020
     **************************************************************************************************/

    public class Stack
    {
        /** \brief   Represents the memory */
        private readonly Memory memory;

        /** \brief   Represents the stack pointer */
        public short pointer;

        /** \brief   Represents the high byte of the stack */
        public short stackhigh = 0x1FF;

        /** \brief   Represents the low byte of the stack */
        public short stacklow = 0x100;

        /**********************************************************************************************//**
         * \fn  public Stack(Memory memory)
         *
         * \brief   Initializes a new instance of the <see cref="Stack" /> class.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   memory  The memory.
         **************************************************************************************************/

        public Stack(Memory memory)
        {
            this.memory = memory;
            pointer = 0x1FF;
        }

        /**********************************************************************************************//**
         * \fn  public void Push(byte value)
         *
         * \brief   Pushes a new value onto the stack
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \exception   StackFullException  Thrown when a Stack Full error condition occurs.
         *
         * \param   value   The value.
         *
         * ### exception    SystemEmulator.StackFullException   .
         **************************************************************************************************/

        public void Push(byte value)
        {
            if (isFull()) throw new StackFullException();

            pointer -= 1;
            memory.WriteMemory(pointer, value);
        }

        /**********************************************************************************************//**
         * \fn  public byte Pop()
         *
         * \brief   Returns the item that the pointer is pointing to and move the pointer to the next
         *          value.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \exception   StackEmptyException Thrown when a Stack Empty error condition occurs.
         *
         * \returns System.Byte.
         *
         * ### exception    SystemEmulator.StackEmptyException  .
         **************************************************************************************************/

        public byte Pop()
        {
            if (isEmpty()) throw new StackEmptyException();

            var retvalue = memory.DirectReadMemory(pointer);
            pointer += 1;
            return retvalue;
        }

        /**********************************************************************************************//**
         * \fn  public byte Peek()
         *
         * \brief   Peeks and returns the item on top of the stack without changing the pointer
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \exception   StackEmptyException Thrown when a Stack Empty error condition occurs.
         *
         * \returns System.Byte.
         *
         * ### exception    SystemEmulator.StackEmptyException  .
         **************************************************************************************************/

        public byte Peek()
        {
            if (isEmpty()) throw new StackEmptyException();
            var retvalue = memory.DirectReadMemory(pointer);
            return retvalue;
        }

        /**********************************************************************************************//**
         * \fn  public bool isEmpty()
         *
         * \brief   Determines whether this instance is empty.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \returns <c>true</c> if this instance is empty; otherwise, <c>false</c>.
         **************************************************************************************************/

        public bool isEmpty()
        {
            if (pointer == stackhigh)
                return true;
            return false;
        }

        /**********************************************************************************************//**
         * \fn  public bool isFull()
         *
         * \brief   Determines whether this instance is full.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \returns <c>true</c> if this instance is full; otherwise, <c>false</c>.
         **************************************************************************************************/

        public bool isFull()
        {
            if (pointer == stacklow)
                return true;
            return false;
        }

        /**********************************************************************************************//**
         * \fn  public byte GetPointer()
         *
         * \brief   Gets the pointer.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \returns System.Byte.
         **************************************************************************************************/

        public byte GetPointer()
        {
            return (byte) (pointer - 0x100);
        }

        /**********************************************************************************************//**
         * \fn  public void SetPointer(byte value)
         *
         * \brief   Sets the pointer.
         *
         * \author  Glenn
         * \date    02/02/2020
         *
         * \param   value   The value.
         **************************************************************************************************/

        public void SetPointer(byte value)
        {
            pointer = (short) (value + 0x100);
        }
    }
}